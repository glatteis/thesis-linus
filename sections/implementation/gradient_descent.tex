\subsection{Gradient Descent variant}\label{section:impl_gradient_descent}

The algorithm we implemented (see \cref{alg:impl_gradient_descent_on_mc})
is a \emph{monotone} Stochastic Gradient Descent algorithm with \emph{step size adaptation}
\cite{gradient_notes}.
Step size adaptation means that this algorithm keeps a unique step size for every parameter.
Here, the initial step size is 0.25 for all parameters.
In every step, the algorithm chooses a parameter \(p \in V\), computes the sign of the
derivative w.r.t.\ \(p\) and steps into the derivative's direction with the current step size.
The size of the derivative is ignored, only the sign is used.
After stepping w.r.t.\ \(p\), the reachability probability from the new position is computed.
If the step was good, meaning it changed the reachability probability into the desired direction,
the step size is increased (here, with a factor of 1.2). Otherwise, it is decreased (here,
with a factor of 0.5) and the position is reset to the previous position. This is the meaning of
monotone: the reachability probability at the current position never changes to a less desirable
value, because steps that would have this effect are undone.

We chose this variant of Gradient Descent because we observed that if we take the size of
the derivative into account, the algorithm gets stuck on saddle points in some case studies and gets
stuck on the edge of the search space on other case studies.
This also happened to variants of Batch Gradient Descent that we considered. Using monotone Gradient Descent
alongside step size adaptation is recommended in \cite{gradient_notes}, and we also observed
that monotone Gradient Descent converges faster on some case studies.

\begin{algorithm}
	\hspace*{\algorithmicindent} \textbf{Input:} pMC \(\MC\) with variables \(V\), initial state \(s_0\),
	target state \(\good\), starting point \(u_0: V \rightarrow \mathbb{R}\), tolerance
	\(\theta\), accuracy \(\varepsilon\) \\
	\hspace*{\algorithmicindent} \textbf{Output:} some instantiation \(u: V \rightarrow \mathbb{R}\) that is
		an approximate local maximum or minimum of \(\Pr_{\MC}(s_0 \vDash \diamondsuit \good)\)
	\begin{algorithmic}[1]
		\State \(u \leftarrow u_0\)
		\Comment{set position as starting point}
		\State{\(s(p) \leftarrow 0.25 \text{ for all } p \in V\)}
		\Comment{initialize step size as 0.25}
		\State{\(prob_{old} \leftarrow \) \text{approximateProbability}(\(\MC[u], \good\))}
		\Comment{approximate \(\Pr_{\MC[u]}(s_0 \vDash \diamondsuit \good)\)}
		\Repeat
		\State{\(p \leftarrow \text{next parameter in repeating queue}\)}
		\State{\((\pt{} x_0, \ldots, \pt{} x_{n-1})^T \leftarrow\)
		\text{approximateDerivative}(\(\MC[u], \good, p\))}
		\Comment{
			see \cref{subsection:approximation}
		}
		\State{\(u_{old} \leftarrow u\)}
		\Comment{
			store old value of \(u\) to use if we undo the step
		}
		\State{\(u(p) \leftarrow u(p) \pm s(p) \cdot \text{sgn}(\pt{} x_0)\)}
		\Comment{update step for parameter}

		\State{\(prob_{new} \leftarrow \) \text{approximateProbability}(\(\MC[u], \good\))}
		\If{maximizing and \(prob_{new} \geq prob_{old}\) or minimizing and \(prob_{new} \leq prob_{old}\)}
			\State{\(s(p) \leftarrow s(p) \cdot 1.2\)}
			\Comment{the step was good, increase step size}
			\State{\(prob_{old} \leftarrow prob_{new}\)}
		\Else
			\State{\(s(p) \leftarrow s(p) \cdot 0.5\)}
			\Comment{the step was bad, decrease step size}
			\State{\(u \leftarrow u_{old}\)}
			\Comment{undo step}
		\EndIf

		\State{\(u(p) \leftarrow \max(\varepsilon, \min(1-\varepsilon, u(p))\) for all \(p \in V\)}
		\Comment{ensure \(u\) is well-defined and graph-preserving}

		\Until \(s(p) \cdot \text{sgn}(\pt{} x_0) < \theta\) for \(2 \cdot |V|\) iterations in sequence
	\end{algorithmic}
	\caption{Monotone Gradient Descent with step size adaptation on a pMC}\label{alg:impl_gradient_descent_on_mc}
\end{algorithm}

We have not thoroughly benchmarked other Gradient Descent variants yet, and it is likely that there
exist techniques that lead to faster convergence for our use case than monotone step size adaptation
Stochastic Gradient Descent. Fine-tuning this algorithm is a topic for further research.  It is also
possible that faster methods for approximating the derivative can be found, especially because the
algorithm only uses its sign whereas its value is computed to a much greater precision than needed.
