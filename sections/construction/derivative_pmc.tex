\subsection{Derived pMCs}\label{subsection:derived_pmcs}

The \emph{Derived pMC} extends the notion of deriving reachability equation systems to deriving the
pMC itself.  After converting a pMC into its Derived pMC, the derivative of the reachability
function can be computed in the standard way (like in \cref{section:exact}) instead of using the
method presented in \cref{subsection:gradient_computation}.

Recall that the linear system of equations in \cref{def:system_of_equations_derivative} to compute
the derived reachability function stems from the product and sum rules applied to its underived
counterpart. Intuitively, the Derived pMC \(\dervar{p} \MC\) of a pMC \(\MC\) stems from the product
and sum rules being applied directly to \(\MC\).  \enquote{Deriving} all states \(s \in S_{\MC}\)
this way with respect to \(p \in V\) yields new states \(\dervar{p} s\), so the size of \(\dervar{p}
\MC\) is twice the size of \(\MC\).  For every transition \(\Prob(s, s') \neq 0\) for \(s, s' \in
S_{\MC}\), we \enquote{use the product rule} and add the transitions \(\Prob(\dervar{p} s,
\dervar{p} s') = \Prob(s, s') \) and \(\Prob(\dervar{p} s, s') = \dervar{p} \Prob(s, s')\)
to \(\dervar{p} \MC\).\footnote{
	Using the semiring of the polynomials with a derivation operation, the Derived pMC can be
	defined more generally.
}

\begin{definition}[Derived pMC]\label{def:derivative_automaton}
	Given a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\) and a parameter \(p \in V\),
	the Derived pMC \(\pt{}  \MC = (S', V, \dervar{p} s_0, \Prob', AP, L)\)
	has the additional states \(\pt{}  S = \{\pt{}  s \mid s \in S\}\)
	and is constructed as follows:
	\begin{align*}
		S' &= S \, \dot{\cup} \, \pt{}  S, \\
		\Prob'(s, t) &= \begin{cases}
			\Prob(s, t) & \text{if } s, t \in S \text{ or } s, t \in \pt{}  S, \\
			\pt{}  \Prob(s, t) & \text{if } s \in \dervar{p} S \text{ and } t \in S, \\
			0 & \text{if } s \in S \text{ and } t \in \dervar{p} S.
		\end{cases}
	\end{align*}
\end{definition}

% \begin{remark}
% 	One can define the Derived pMC for a pMC \(\MC\) for multiple parameters either by adding states and
% 	transitions as above for every parameter in \(V_{\MC}\) or by constructing one Derived pMC
% 	for every parameter.
% \end{remark}

\begin{figure}
	\begin{center}
		\begin{tikzpicture}
			\node[state, initial] (s0') {$\partial s_0$};
			\node[state, below of=s0'] (s1') {$\partial s_1$};
			\node[state, below of=s1'] (s2') {$\partial \good$};
			\node[state, left of=s1'] (s3') {$\partial \bad$};
			
			\node[state, right = 5cm of s0'] (s0) {$s_0$};
			\node[state, below of=s0] (s1) {$s_1$};
			\node[state, below of=s1] (s2) {$\good$};
			\node[state, left of=s1] (s3) {$\bad$};

			\draw 	(s0) edge node[auto] {$p$} (s1)
				(s0) edge node[above left] {$1-p$} (s3)
				(s1) edge node[auto] {$1-p$} (s2)
				(s1) edge[below] node{$p$} (s3)
				(s2) edge[loop right] node[auto] {$1$} (s2)
				(s3) edge[loop below] node[auto] {$1$} (s3)
				% derivative
				(s0') edge node[left] {$p$} (s1')
				(s0') edge node[auto] {$1$} (s1)
				(s0') edge node[left] {$1-p$} (s3')
				(s0') edge[bend right] node[above] {$-1$} (s3)

				(s1') edge node[left] {$1-p$} (s2')
				(s1') edge[bend right] node[above] {$-1$} (s2)
				(s1') edge node[auto] {$p$} (s3')
				(s1') edge[bend right] node[auto] {$1$} (s3)
				(s2') edge[loop left] node[auto] {$1$} (s2')
				(s3') edge[loop left] node[auto] {$1$} (s3')
				;
		\end{tikzpicture}
	\end{center}
	\caption{The Derived pMC of \cref{fig:gradient_construction_mc} w.r.t.\ \(p\)}\label{example:derivative_pmc}
\end{figure}

\begin{example}
	\cref{example:derivative_pmc} shows the Derived pMC of the pMC shown in
	\cref{fig:gradient_construction_mc}.  Constructing the linear equation system in
	\cref{def:system_of_equations} on the pMC in \cref{example:derivative_pmc}, we can compute
	the derived reachability function \(\Pr(\partial s_0 \vDash \diamondsuit \good) = (1-p) -
	p\). For readability, we have not merged all states satisfying \(\lnot \diamondsuit \good\)
	into one state in this example. To do this, one would have to merge \(\partial \bad,
	\partial \good\), and \(\bad\) into \(\bad\).
\end{example}

Note that most Derived pMCs (like the pMC in \cref{example:derivative_pmc}) have no well-defined
instantiation, as they have transitions that never yield probabilities when instantiated. 
This means that we have not shown yet that
the system of equations in \cref{def:system_of_equations} that solves for the reachability function 
always has a unique solution when applied to a Derived pMC.
As we have shown that the derived equation system
\cref{def:system_of_equations_derivative} has a unique solution, we show this by proving that
constructing the system of equations in \cref{def:system_of_equations} on a Derived pMC yields
an instance of the system of equations in \cref{def:system_of_equations_derivative}.

\begin{theorem}
	Given an pMC \(\MC\), its states \(S = \{s_0, s_1, \ldots, s_{n-1}\}\), a parameter \(p \in V\)
	and a target state \(\good\),
	constructing the standard reachability system of equations in \cref{theorem:equiv_system_exact}
	on the Derived pMC \(\pt{}  \MC\)
	yields the derived reachability system of equations in
	\cref{theorem:system_of_equations_derivative_matrix} on \(\MC\).

	\begin{proof}
		Let \(S\) be the underived states and \(\pt S\) be the derived states in \(\pt
		\MC\). Consider all states from which \(\good\) is not reachable merged into a
		single state \(\bad\).  We construct the system of equations
		\cref{theorem:equiv_system_exact} on \(\pt \MC\):
		\begin{align*}
			x_{\bad} &= 0 & \\
			x_{\good} &= 1 & \\
			x_s &= \sum_{s' \in S \cup \pt S} \Prob(s, s') \cdot x_{s'} &\text{ for } s
			\in (S \cup \pt S) \setminus \{\good, \bad\}
		\end{align*}
		We expand the last line for states in \(\pt S\) and observe that \(\Prob(s, s') =
		0\) for \(s \in S, s' \in \pt S\).
		\begin{align*}
			x_{\bad} &= 0  \\
			x_{\good} &= 1  \\
			x_s &= \sum_{s' \in S} \Prob(s, s') \cdot x_{s'} &\text{ for } s
			\in S \setminus \{\good, \bad\} \\
			x_{\pt s} &= \sum_{s' \in S} \pt \Prob(s, s') \cdot x_{s'} + \Prob(s, s') \cdot
			x_{\pt s'} &\text{ for } s
			\in S \setminus \{\good, \bad\}
		\end{align*}
		After renaming the variables \(x_{\pt s}\) to \(\pt x_s\)
		and setting \(\pt x_{\bad} = \pt x_{\good} = 0\), this is the derived system of
		equations \cref{theorem:system_of_equations_derivative_matrix} on \(\MC\).
	\end{proof}
\end{theorem}
