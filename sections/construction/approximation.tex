\subsection{Approximation of the derivative}\label{subsection:approximation}

The derivative of the reachability function at a given instantiation \(u\)
can be approximated via \emph{value iteration} \cite{optimistic_value_iteration},
which is a method that can be used to approximate reachability probabilities in MCs.
Recall \cref{theorem:derivative_system_unique}: \[
	\begin{pmatrix}
		\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
	\end{pmatrix}
	=
	(1 - A)^{-1}
	\left(
	(\dervar{p} A)
	(1 - A)^{-1}
	\begin{pmatrix}
		\Prob(s_0, \good) \\ 
		\Prob(s_1, \good) \\ 
		\vdots \\
		\Prob(s_{n-1}, \good) \\ 
	\end{pmatrix}
	+
	\begin{pmatrix}
		\pt{}  \Prob(s_0, \good) \\ 
		\pt{}  \Prob(s_1, \good) \\ 
		\vdots \\
		\pt{}  \Prob(s_{n-1}, \good) \\ 
	\end{pmatrix}
	\right)
\]
We now approximate the solution to this equation without having to calculate \((1-A)^{-1}\).
Recall that \(A\) is the transition matrix of a pMC \(\MC\) where the states \(\good\) and \(\bad\) are
omitted. If \(\MC\) is an MC, it holds that \(
	(1-A)^{-1} = \sum_{i=0}^{\infty} A^i
\) (we prove this in \cref{theorem:approximation}). Using this fact, we can approximate the solution of \((1-A)^{-1} v\)
for \(A \in \mathbb{R}^{n \times n}\) and \(v \in \mathbb{R}^n\) in a variant of value iteration as follows.
We create two temporary vectors \(s = 0\) and \(x = v\) and iteratively execute the steps
\(s \leftarrow s + x\) and \(x \leftarrow A \cdot x\). This means that after the \(i\)th step of the
procedure (starting at zero), \(x = A^i v\) and \(s = \sum_{j=0}^i A^i v\).
We can stop this procedure when \(|x| \leq \theta\) for a threshold \(\theta\). The result of the
computation is \(s \approx \sum_{i=0}^{\infty} A^i v = (1-A)^{-1}\).

Let us call this procedure \(\text{valueIterate}(A, v)\). Then we can approximate the derivative of
the reachability function by computing the above equation while swapping all multiplications of
vectors with \((1-A)^{-1}\) with a call to valueIterate.
This algorithm to approximate the derivative at an
instantiation \(u\) is presented in \cref{alg:der_value_iteration}.

\begin{algorithm}
	\begin{algorithmic}[1]
		\State \(v_{underived} \leftarrow
		\begin{pmatrix}
			\Prob(s_0, \good) \\ 
			\Prob(s_1, \good) \\ 
			\vdots \\
			\Prob(s_{n-1}, \good) \\ 
		\end{pmatrix}\)
		\State \(v_{derived} \leftarrow
		\begin{pmatrix}
			\pt{}  \Prob(s_0, \good) \\ 
			\pt{}  \Prob(s_1, \good) \\ 
			\vdots \\
			\pt{}  \Prob(s_{n-1}, \good) \\ 
		\end{pmatrix}\)
		\State \(v_{tmp} \leftarrow \) valueIterate(\(A, v_{underived}\))
		\Comment Approximate \((1-A)^{-1} v_{underived}\) using value iteration
		\State \(v_{tmp} \leftarrow \) \(\dervar{p} A \cdot v_{tmp}\)
		\State \(v_{tmp} \leftarrow \) \(v_{tmp} + v_{derived}\)
		\State \(v_{tmp} \leftarrow \) valueIterate(\(A, v_{tmp}\))
		\Comment Approximate \((1-A)^{-1} v_{tmp}\) using value iteration
		\State \Return \(v_{tmp}\)
	\end{algorithmic}
	\caption{Approximating the derivative using value iteration}\label{alg:der_value_iteration}
\end{algorithm}

\begin{theorem}\label{theorem:approximation}
	Let a pMC \(\MC\) and a target \(\good\) be given and let \(A \in \mathbb{R}^{n \times n}\)
	be the transition matrix of
	\(\MC\) in which the states \(\good\) and \(\bad\) are omitted
	(as defined in \cref{theorem:equiv_system_exact}).
	Let \(\good\) be reachable from \(s_0\), but \(\good \neq s_0\).
	Then, \[(1-A)^{-1} = \sum_{i=0}^{\infty} A^i.\]
	\begin{proof}
		This is proved in \cite[767]{baier-katoen}.
	\end{proof}
\end{theorem}
