\subsection{Computing the derivative using a linear system of
equations}\label{subsection:gradient_computation}

To use Gradient Descent (see \cref{alg:plain_gradient_descent}) to find a local maximum
or minimum of the reachability probability in pMCs, we construct
a linear system of equations that solves directly to the derivative of the
reachability function. This is due to \cite{moermanblog}. We start with an example that illustrates how to obtain the derivative of the reachability
function with respect to a parameter.

\begin{example}\label{example:construction_1}
	To compute the derivative of the reachability function with respect to a parameter \(p \in
	V\), we construct the system of equations in \cref{def:system_of_equations} and derive every
	term with respect to \(p\) using the product rule. The product rule says that for two
	differentiable functions \(f\) and \(g\), \(\dervar{x} (f(x) \cdot g(x)) = \dervar{x} f(x)
	\cdot g(x) + f(x) \cdot \dervar{x} g(x)\).  Consider the term \(x_0 = p \cdot x_1 + (1-p)
	\cdot x_{\bad}\) in \cref{example:exact_1}.  To derive this term with respect to \(p\), we
	use the product rule on the first sum and on the second sum, according to the sum rule. This
	yields the term \(\dervar{p}{x_0} = 1 \cdot x_1 + p \cdot \dervar{p}{x_1} + (-1) \cdot
	x_{\bad} + (1-p) \cdot \dervar{p}{x_{\bad}}\).  Here, we we can view \(\pt{}  x_{i}\) as
	variables part of the derived system of equations.  This way of deriving equations is called
	implicit differentiation.

	Applied to the pMC in \cref{figure:exact_pmc}, the complete system of
	equations derived w.r.t.\ \(p\) looks as follows. We have colored all variables for readability.
	% \begin{align*}
	% 	\rv{x_0} &= p \cdot \rv{x_1} + (1-p) \cdot \rv{x_{\bad}} \\
	% 	\rv{x_1} &= (1-p) \cdot \rv{x_{\good}} + p \cdot \rv{x_{\bad}} \\
	% 	\rv{x_{\good}} &= 1 \\
	% 	\rv{x_{\bad}} &= 0 \\
	% 	\mbdervar{p}{x_0} &= p \cdot \mbdervar{p}{x_1} + \rv{x_1} + (1-p) \cdot \mbdervar{p}{x_{\bad}} - \rv{x_{\bad}} \\
	% 	\mbdervar{p}{x_1} &= (1-p) \cdot \mbdervar{p}{x_{\good}} - \rv{x_{\good}} + p \cdot \mbdervar{p}{x_{\bad}} + \rv{x_{\bad}} \\
	% 	\mbdervar{p}{x_{\good}} &= 0 \\
	% 	\mbdervar{p}{x_{\bad}} &= 0
	% \end{align*}
	\begingroup\allowdisplaybreaks
	\begin{align*}
		\rv{x_{\bad}} &= 0 \\
		\rv{x_{\good}} &= 1 \\
		\rv{x_0} &= p \cdot \rv{x_1} + (1-p) \cdot \rv{x_{\bad}} \\
		\rv{x_1} &= q \cdot \rv{x_{\good}} + (1-q) \cdot \rv{x_{\bad}} \\
		\mbdervar{p}{x_{\bad}} &= 0 \\
		\mbdervar{p}{x_{\good}} &= 0 \\
		\mbdervar{p}{x_0} &= 1 \cdot \rv{x_1} + p \cdot \mbdervar{p}{x_1} + (-1) \cdot \rv{x_{\bad}}
		+ (1-p) \cdot \mbdervar{p}{x_{\bad}} \\
		\mbdervar{p}{x_1} &= 0 \cdot \rv{x_{\good}} + q \cdot \mbdervar{p}{x_{\good}} +
		0 \cdot \rv{x_{\bad}} + (1-q) \cdot \mbdervar{p}{x_{\bad}}
	\end{align*}
	\endgroup
	The first four lines are the equation system in \cref{def:system_of_equations}.
	The last four lines are the derivatives of the first four lines w.r.t.\ \(p\).
	Solving this system of equations, we obtain
	\(\rv{x_0} = p \cdot q, \rv{x_1} = q, \mbdervar{p}{x_0} = q\) and \(\mbdervar{p}{x_1} = 0\).
\end{example}

\newcommand\systemofequationsderivativedef{
\begin{alignat*}{2}
	\rv{x_{\bad}} &= 0 && \\
	\rv{x_{\good}} &= 1 && \\
	\rv{x_s} &= \sum_{s' \in S} \Prob(s, s') \cdot \rv{x_{s'}} && \text{for } s
	\in S \setminus \{\good, \bad\} \\
	\mb{\pt{}  x_{\bad}} &= 0 && \\
	\mb{\pt{}  x_{\good}} &= 0 && \\
	\mb{\pt{}  x_s} &= \sum_{s' \in S} \left(
		\pt{}  \Prob(s, s') \cdot \rv{x_{s'}} +
	\Prob(s, s') \cdot \mb{\pt{}  x_{s'}} \right) \; \;
			&&\text{for } s \in S \setminus \{\good, \bad\}
	\end{alignat*}
}
\newcommand\systemofequationsderivativedefwithlabels{
\begin{alignat}{2}
	\rv{x_{\bad}} &= 0 && \label{line:der:1} \\
	\rv{x_{\good}} &= 1 && \label{line:der:2} \\
	\rv{x_s} &= \sum_{s' \in S} \Prob(s, s') \cdot \rv{x_{s'}} && \text{for } s \in S \setminus \{\good, \bad\}
	\label{line:der:3} \\
	\mb{\pt{}  x_{\bad}} &= 0 && \\
	\mb{\pt{}  x_{\good}} &= 0 && \label{line:der:4} \\
	\mb{\pt{}  x_s} &= \sum_{s' \in S} \left(
		\pt{}  \Prob(s, s') \cdot \rv{x_{s'}} +
	\Prob(s, s') \cdot \mb{\pt{}  x_{s'}} \right) \; \;
					     &&\text{for } S \setminus \{\good, \bad\} \label{line:der:5}
	\end{alignat}
}
\begin{definition}\label{def:system_of_equations_derivative}
	Given a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\)
	and a parameter \(p \in V\), 
	the probability to eventually reach \(\good\) from any state \(s \in S\) and the respective
	partial derivative
	w.r.t.\ \(p\) is computed using the following system of equations:
	\systemofequationsderivativedef
	The variables are \(\rv{x_s}\) and \(\mb{\pt{}  x_s}\) for \(s \in S\).
	\(\pt{}  \Prob(s, s')\) is the derivative of \(\Prob(s, s')\) w.r.t.\ \(p\).
\end{definition}

Again, we convert the system of equations in \cref{def:system_of_equations_derivative} into
the form \(\hat{A}\hat{x}=\hat{b}\).

\begin{theorem}\label{theorem:system_of_equations_derivative_matrix}
	Consider a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\), a target \(\good\),
	and a parameter \(p \in V\).  Let \(\widetilde{S} = S \setminus \{\good, \bad\} =
	\{s_0, s_1, \ldots, s_{n-1}\}\). Let \(A\) be the constrained transition matrix for the
	states in 
	\(\widetilde{S}\), and \(\pt{}  A\) the derivative of \(A\) w.r.t.\
	\(p\) in every entry: (\((\dervar{p} A)_{i, j} = \dervar{p}{(A_{i, j}})\)).  Then, the
	system of equations in \cref{def:system_of_equations_derivative} has the same solutions
	for states in \(\widetilde{S}\) as the linear system of equations
	\begin{align*}
& \left(1 - \left(
		\begin{array}{c|c}
			A
	&
	0
	\\ \hline
	\pt{} {A}
	&
	A
	\\
		\end{array}
\right) \right)
\begin{pmatrix}
	\rv{x_0} \\ \rv{x_1} \\ \vdots \\ \rv{x_{n-1}} \\
	\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
\end{pmatrix}
=
\begin{pmatrix}
	\Prob(s_0, \good) \\ 
	\Prob(s_1, \good) \\ 
	\vdots \\
	\Prob(s_{n-1}, \good) \\ 
	\pt{}  \Prob(s_0, \good) \\ 
	\pt{}  \Prob(s_1, \good) \\ 
	\vdots \\
	\pt{}  \Prob(s_{n-1}, \good) \\ 
\end{pmatrix} \\
	\end{align*}
	\begin{proof}
		Recall \cref{def:system_of_equations_derivative}:
		\systemofequationsderivativedefwithlabels
		We rearrange \cref{line:der:5} into two parts. This yields:
		\[
			\mb{\pt{}  x_s} = \sum_{s' \in S}
			\pt{}  \Prob(s, s') \cdot \rv{x_{s'}} + \sum_{s' \in S}
			\Prob(s, s') \cdot \mb{\pt{}  x_{s'}}
			\text{ for } s \in S \setminus \{\good, \bad\}
		\]
		The second sum is just a copy of the first system of equations, but over the
		derivatives of the variables.  The first sum relates back to the non-derived
		variables, but with derivatives over the probabilities.  Because the non-derived
		variables also appear in this system of equations, we cannot solve it for itself and
		must solve it together with the original system of equations.

		Let \(\widetilde{S}\) be given as above.
		Replacing \(x_{\bad}\) with \(0\) and \(x_{\good}\) with \(1\),
		we can write the system of equations as
		\begin{align*}
			\begin{pmatrix}
				\rv{x_0} \\ \rv{x_1} \\ \vdots \\ \rv{x_{n-1}} \\
				\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
			\end{pmatrix}
			=& 
			\begin{pmatrix}
				\sum_{i = 0}^{n-1} \Prob(s_0, s_i) \rv{x_i} \\
				\sum_{i = 0}^{n-1} \Prob(s_1, s_i) \rv{x_i} \\
				\vdots \\
				\sum_{i = 0}^{n-1} \Prob(s_{n-1}, s_i) \rv{x_i} \\
				\sum_{i = 0}^{n-1} (\pt{}  \Prob(s_0, s_i) \rv{x_i} +
				\Prob(s_0, s_i) \mbdervar{p}{x_i})
				\\
				\sum_{i = 0}^{n-1} (\pt{}  \Prob(s_1, s_i) \rv{x_i} +
				\Prob(s_1, s_i) \mbdervar{p}{x_i})
				\\
				\vdots
				\\
				\sum_{i = 0}^{n-1} (\pt{}  \Prob(s_{n-1}, s_i) \rv{x_i} +
				\Prob(s_{n-1}, s_i) \mbdervar{p}{x_i})
				\\
			\end{pmatrix}
			+
			\begin{pmatrix}
				\Prob(s_0, \good) \\ 
				\Prob(s_1, \good) \\ 
				\vdots \\
				\Prob(s_{n-1}, \good) \\ 
				\pt{}  \Prob(s_0, \good) \\ 
				\pt{}  \Prob(s_1, \good) \\ 
				\vdots \\
				\pt{}  \Prob(s_{n-1}, \good) \\ 
			\end{pmatrix}
		\end{align*}
		Splitting the sums up as before and converting them into a matrix
		multiplication, one can see that this is equivalent to
		\begin{align*}
			\begin{pmatrix}
				\rv{x_0} \\ \rv{x_1} \\ \vdots \\ \rv{x_{n-1}} \\
				\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
			\end{pmatrix}
			=& 
			\left(
				\begin{array}{c|c}
					A
		&
		0
		\\ \hline
		\pt{} {A}
		&
		A
		\\
				\end{array}
			\right)
			\begin{pmatrix}
				\rv{x_0} \\ \rv{x_1} \\ \vdots \\ \rv{x_{n-1}} \\
				\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
			\end{pmatrix}
			+
			\begin{pmatrix}
				\Prob(s_0, \good) \\ 
				\Prob(s_1, \good) \\ 
				\vdots \\
				\Prob(s_{n-1}, \good) \\ 
				\pt{}  \Prob(s_0, \good) \\ 
				\pt{}  \Prob(s_1, \good) \\ 
				\vdots \\
				\pt{}  \Prob(s_{n-1}, \good) \\ 
			\end{pmatrix}
		\end{align*}
		where \((\dervar{p} A)_{i, j} = \dervar{p}{(A_{i, j}})\).
		The above equation is equivalent to the equation in
		\cref{theorem:system_of_equations_derivative_matrix}.
	\end{proof}
\end{theorem}

\begin{example}\label{example:construction_2}
	Recalling the pMC in \cref{figure:exact_pmc}, the system of equations from
	\cref{theorem:system_of_equations_derivative_matrix} is as follows:
	\[
		\begin{pmatrix}
			1 & -p & 0 & 0 \\
			0 & 1 & 0 & 0 \\
			0 & -1 & 1 & -p \\
			0 & 0 & 0 & 1 
		\end{pmatrix}
		\begin{pmatrix}
			\rv{x_0} \\ \rv{x_1} \\ \mbdervar{p}{x_0} \\ \mbdervar{p}{x_1}
		\end{pmatrix}
		=
		\begin{pmatrix}
			0 \\ q \\ 0 \\ 0
		\end{pmatrix}
	\]
	Solving this system of equations, we (again) obtain
	\(\rv{x_0} = p \cdot q, \rv{x_1} = q, \mbdervar{p}{x_0} = q\) and \(\mbdervar{p}{x_1} = 0\).
\end{example}

\begin{theorem}\label{theorem:derivative_system_unique}
	Given a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\), a target \(\good\), and a parameter \(p \in
	V\), the system of equations constructed in
	\cref{theorem:system_of_equations_derivative_matrix} has exactly one solution.

	\begingroup\allowdisplaybreaks
	\begin{proof}
		Let \(A\) and \(\widetilde{S} = \{s_0, s_1, \ldots, s_{n-1}\}\) be defined as in
		\cref{theorem:system_of_equations_derivative_matrix}.
		For the variables \(\rv{x_0} , \rv{x_1} , \ldots , \rv{x_{n-1}}\):
		\[
			(1 - A)
			\begin{pmatrix}
				\rv{x_0} \\ \rv{x_1} \\ \vdots \\ \rv{x_{n-1}}
			\end{pmatrix}
			=
			\begin{pmatrix}
				\Prob(s_0, \good) \\ 
				\Prob(s_1, \good) \\ 
				\vdots \\
				\Prob(s_{n-1}, \good) \\ 
			\end{pmatrix}
			\Leftrightarrow
			\begin{pmatrix}
				\rv{x_0} \\ \rv{x_1} \\ \vdots \\ \rv{x_{n-1}}
			\end{pmatrix}
			=
			(1 - A)^{-1}
			\begin{pmatrix}
				\Prob(s_0, \good) \\ 
				\Prob(s_1, \good) \\ 
				\vdots \\
				\Prob(s_{n-1}, \good) \\ 
			\end{pmatrix}
		\]
		\((1 - A)^{-1}\) exists because \(1-A\) is invertible (this follows from \cref{theorem:unique_solution_book}).

		For the variables \(\mbdervar{p}{x_0} , \mbdervar{p}{x_1} , \ldots , \mbdervar{p}{x_{n-1}}\):
		\begin{align*}
			\begin{pmatrix}
				\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
			\end{pmatrix}
			=&
			(\dervar{p} A)
			\begin{pmatrix}
				\rv{x_0} \\ \rv{x_1} \\ \vdots \\ \rv{x_{n-1}}
			\end{pmatrix}
			+
			A
			\begin{pmatrix}
				\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
			\end{pmatrix}
			+
			\begin{pmatrix}
				\pt{}  \Prob(s_0, \good) \\ 
				\pt{}  \Prob(s_1, \good) \\ 
				\vdots \\
				\pt{}  \Prob(s_{n-1}, \good) \\ 
			\end{pmatrix} \\
			\Leftrightarrow
			(1-A)
			\begin{pmatrix}
				\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
			\end{pmatrix}
			=&
			(\dervar{p} A)
			\begin{pmatrix}
				\rv{x_0} \\ \rv{x_1} \\ \vdots \\ \rv{x_{n-1}}
			\end{pmatrix}
			+
			\begin{pmatrix}
				\pt{}  \Prob(s_0, \good) \\ 
				\pt{}  \Prob(s_1, \good) \\ 
				\vdots \\
				\pt{}  \Prob(s_{n-1}, \good) \\ 
			\end{pmatrix} \\
			\Leftrightarrow
			(1-A)
			\begin{pmatrix}
				\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
			\end{pmatrix}
			=&
			(\dervar{p} A)
			(1 - A)^{-1}
			\begin{pmatrix}
				\Prob(s_0, \good) \\ 
				\Prob(s_1, \good) \\ 
				\vdots \\
				\Prob(s_{n-1}, \good) \\ 
			\end{pmatrix}
			+
			\begin{pmatrix}
				\pt{}  \Prob(s_0, \good) \\ 
				\pt{}  \Prob(s_1, \good) \\ 
				\vdots \\
				\pt{}  \Prob(s_{n-1}, \good) \\ 
			\end{pmatrix} \\
			\Leftrightarrow
			\begin{pmatrix}
				\mbdervar{p}{x_0} \\ \mbdervar{p}{x_1} \\ \vdots \\ \mbdervar{p}{x_{n-1}}
			\end{pmatrix}
			=&
			(1 - A)^{-1}
			\left(
				(\dervar{p} A)
				(1 - A)^{-1}
				\begin{pmatrix}
					\Prob(s_0, \good) \\ 
					\Prob(s_1, \good) \\ 
					\vdots \\
					\Prob(s_{n-1}, \good) \\ 
				\end{pmatrix}
				+
				\begin{pmatrix}
					\pt{}  \Prob(s_0, \good) \\ 
					\pt{}  \Prob(s_1, \good) \\ 
					\vdots \\
					\pt{}  \Prob(s_{n-1}, \good) \\ 
				\end{pmatrix}
			\right)
			\\
		\end{align*}
		Thus there exists a unique solution of the system of equations.
	\end{proof}
	\endgroup
\end{theorem}
