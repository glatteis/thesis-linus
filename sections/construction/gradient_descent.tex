\subsection{A Gradient Descent algorithm using the derived system of
equations}\label{subsection:construction_gradient_descent}
With this system of equations, we now sketch an algorithm for Gradient Descent on a pMC in
\cref{alg:plain_gradient_descent_on_mc}.  The algorithm starts at some instantiation \(u_0\).
In every iteration, the algorithm does two things: First, it calculates the derivative of the
reachability function at the current
instantiation with respect to every parameter \(p \in V\) and stores the value for the initial state
\(s_0\) in the vector \(\Delta u\). It then descends in the direction \(\Delta u\) by subtracting
\(\Delta u\) multiplied by the step size from the current position.
This process is repeated until \(|\Delta u| \leq \theta\) for 10 subsequent iterations.
\begin{algorithm}
	\hspace*{\algorithmicindent} \textbf{Input:} a pMC \(\MC\) with parameter space \(V\) and initial state \(s_0\),
	a target state \(\good\), a starting point \(u_0: V \rightarrow \mathbb{R}\),
	step size \(\alpha\), tolerance \(\theta\), accuracy \(\varepsilon\) \\
	\hspace*{\algorithmicindent} \textbf{Output:} some instantiation \(u: V \rightarrow \mathbb{R}\) that is
	an approximate local maximum or minimum of \(\Pr_{\MC}(s_0 \vDash \diamondsuit \good)\)
	\begin{algorithmic}[1]
		\State \(u \leftarrow u_0\)
		\Repeat
		\State{\(\Delta u \leftarrow ()\)}
		\Comment{initialize update vector}
		\For{each parameter \(p \in V\)}
		\Comment{calculate step for every parameter}
		\State{\((\pt{} x_0, \ldots, \pt{} x_{n-1})^T
		\leftarrow\) \text{solveDerivativeSystem}(\(\MC[u], \good, p\))}
		\Comment{
			see \cref{def:system_of_equations_derivative}
			% \(\pt{}  x_0\) is part of the solution to \cref{def:system_of_equations_derivative} in \(\MC[u]\)
		}
		\State{\(\Delta u(p) \leftarrow \pt{} x_0\)}
		\EndFor
		\State{\(u \leftarrow u \pm \alpha \cdot \Delta u\)}
		\Comment{update position (\(+\) if maximizing, \(-\) if minimizing)}
		\State{\(u(p) \leftarrow \max(\varepsilon, \min(1-\varepsilon, u(p))\) for all \(p \in V\)}
		\Comment{ensure \(u\) is well-defined and graph-preserving}
		\Until \(|\Delta u| < \theta\) for \(10\) iterations in sequence
	\end{algorithmic}
	\caption{Batch gradient descent on a pMC (Sketch)}\label{alg:plain_gradient_descent_on_mc}
\end{algorithm}

\begin{example}
	We perform Gradient Descent on the pMC in \cref{fig:gradient_construction_mc} to maximize
	its reachability probability. We illustrate only three exemplary iterations of
	\cref{alg:plain_gradient_descent_on_mc}  We start at \(p \mapsto 0.2\). Solving the system
	of equations \cref{def:system_of_equations_derivative}
	in \(\mathbb{R}\) yields \(\rv{x_0} = 0.16\) and \(\mbdervar{p}{x_0} = 0.6\).
	This tells us that the probability to reach \(\good\) from \(s_0\) is \(0.16\), and the
	derivative of that probability in \(s_0\) with respect to \(p\) is \(0.6\).  As the
	derivative is positive, we would increase \(p\) if we want to maximize the reachability
	probability and decrease \(p\) if we want to minimize it. Because we are maximizing, we
	increase \(p\).

	In turn, when we set \(p \mapsto 0.7\), we get the solution \(\rv{x_0} = 0.21\) and
	\(\mbdervar{p}{x_0} = -0.4\).  This tells us that the probability to reach \(\good\) from
	\(s_0\) with \(p \mapsto 0.7\) is \(0.21\). We need to lower \(p\) to reach a higher
	reachability probability because the derivative of the reachability function from \(s_0\) to
	\(\good\) is negative.

	Finally, when we set \(p \mapsto 0.5\), the solution is \(\rv{x_0} = 0.5\) and
	\(\mbdervar{p}{x_0} = 0\).  Because the derivative of the reachability probability from \(s_0\)
	to \(\good\) is zero, we hit a local maximum (or minimum, or saddle point). The reachability
	probability is \(0.25\).
\end{example}
