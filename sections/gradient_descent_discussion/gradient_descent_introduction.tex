Given a differentiable function \(f: \mathbb{R}^n \rightarrow \mathbb{R}\), \emph{Gradient Descent}
\cite{gradient_overview, gradient_notes} is a method to find a local minimum. For this, we use the
derivatives of the function to calculate the direction of steepest descent.  Then, we take a step of
a certain step size (also called \emph{learning rate}) \(\alpha\) into that direction and reassess
the direction of steepest descent. For a very simple one-dimensional example of a Gradient Descent
algorithm, see \cref{alg:plain_gradient_descent}.

\begin{algorithm}
	\hspace*{\algorithmicindent} \textbf{Input:} starting point \(x_0 \in \mathbb{R}\),
	differentiable function \(f: \mathbb{R} \rightarrow \mathbb{R}\), step size
	\(\alpha \in \mathbb{R}\), tolerance \(\theta \in \mathbb{R}\) \\
	\hspace*{\algorithmicindent} \textbf{Output:} some point \(x \in \mathbb{R}\) that is an
	approximate local maximum or minimum of \(f\)
	\begin{algorithmic}[1]
		\State \(x \leftarrow x_0\)
		\Repeat
		\State \(\Delta x \leftarrow \alpha \dervar{x} f(x)\)
		\State \(x \leftarrow x \pm \Delta x\)
		\Comment{\(+\) if maximizing, \(-\) if minimizing}
		\Until \(\Delta x < \theta\) for 10 iterations in sequence
	\end{algorithmic}
	\caption{Plain one-dimensional gradient descent \cite[see][1]{gradient_notes}}\label{alg:plain_gradient_descent}
\end{algorithm}

This algorithm has a fixed step size that is used as a factor of the derivative. It terminates when
the sizes of the taken steps are under a threshold for 10 subsequent iterations.  Problems with this
particular algorithm include that the proximity of the local minimum is not nessecarily correlated
with the size of the derivative \cite{gradient_notes}, so the algorithm could take a long time to
converge or terminate too early because the steps it is taking are either too large or too small.
It is also possible that this algorithm never terminates.
