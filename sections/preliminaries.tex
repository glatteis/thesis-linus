\section{Preliminaries}

% A (discrete-time) Markov chain is a tuple \(\MC = (S, \mathbf{P}, \iota_{init}, AP, L)\) where
% \begin{itemize}
% 	\item \(S\) is a countable, nonempty set of states,
% 	\item \(\mathbf{P}: S \times S \rightarrow [0, 1]\) is the \emph{transition probability function} such that for all states \(s\):
% 		\[
% 			\sum_{s' \in S} P(s, s') = 1
% 		\]
% 	\item \(\iota_{init}\) is the \emph{initial distribution} such that \(\sum_{s \in S} \iota_{init}(s) = 1\), and
% 	\item \(AP\) is a set of atomic propositions and \(L: S \rightarrow 2^{AP}\) a labeling function.
% \end{itemize}
% \parencite[747f]{baier-katoen}


% \begin{definition}
% 	A \emph{parametric stochastic game (pSG)} is a tuple \(\MC = (S, V, s_0, Act, \Prob)\) with a finite set
% 	\(S\) of states with \(S = S_{\bigcirc} \uplus S_{\square}\), a finite set \(V\) of parameters over \(\mathbb{R}\),
% 	an initial state \(s_0 \in S\), a finite set \(Act\) of actions, and a transition function
% 	\(\Prob: S \times Act \times S \rightarrow \mathbb{R}(V) \cup \mathbb{R} \cup \{\bot\}\)
% 	with \(|Act(s)| \geq 1\) for all \(s \in S\), where \(Act(s) = \{\alpha \in Act \mid \exists s' \in S.
% 	\Prob(s, \alpha, s') \neq \equiv 0\}\) is the state of \emph{enabled} actions at state \(s\).
% 	\begin{itemize}
% 		\item A pSG is a \emph{parametric Markov decision process (pMDP)} if \(S_{\bigcirc} = \emptyset\) or \(S_{\square} = \emptyset\).
% 		\item A pMDP is a \emph{parametric Markov chain (pMC)} if \(|Act(s)| = 1\) for all \(s \in S\).
% 	\end{itemize}

% 	\parencite[6]{parameter-synthesis}.
% \end{definition}

\begin{figure}
	\begin{center}
		\begin{tikzpicture}
			\node[state, initial] (s0) {$s_0$};
			\node[state, below left=1cm and 1.6cm of s0] (s1) {$s_1$};
			\node[state, below right=1cm and 1.6cm of s0] (s2) {$s_2$};
			\node[state, below left=1cm and 0.2cm of s1] (s3) {$s_3$};
			\node[state, below right=1cm and 0.2cm of s1] (s4) {$s_4$};
			\node[state, below left=1cm and 0.2cm of s2] (s5) {$s_5$};
			\node[state, below right=1cm and 0.2cm of s2] (s6) {$s_6$};

			\node[state, below left=1cm and 0.05cm of s3] (roll_1) {\Large \epsdice{1}};
			\node[state, below left=1cm and 0.05cm of s4] (roll_2) {\Large \epsdice{2}};
			\node[state, below right=1cm and 0.05cm of s4] (roll_3) {\Large \epsdice{3}};

			\node[state, below left=1cm and 0.05cm of s5] (roll_4) {\Large \epsdice{4}};
			\node[state, below left=1cm and 0.05cm of s6] (roll_5) {\Large \epsdice{5}};
			\node[state, below right=1cm and 0.05cm of s6] (roll_6) {\Large \epsdice{6}};
			
			\draw 	(s0) edge[] node[above] {$\sfrac{1}{2}$} (s1)
			 	(s0) edge[] node[above] {$\sfrac{1}{2}$} (s2)

			 	(s1) edge[bend right] node[left] {$\sfrac{1}{2}$} (s3)
			 	(s3) edge[bend right] node[left] {$\sfrac{1}{2}$} (s1)
			 	(s1) edge[] node[right] {$\sfrac{1}{2}$} (s4)
			 	(s3) edge[] node[right] {$\sfrac{1}{2}$} (roll_1)
			 	(s4) edge[] node[left] {$\sfrac{1}{2}$} (roll_2)
			 	(s4) edge[] node[right] {$\sfrac{1}{2}$} (roll_3)

			 	(s2) edge[bend right] node[left] {$\sfrac{1}{2}$} (s5)
			 	(s5) edge[bend right] node[left] {$\sfrac{1}{2}$} (s2)
			 	(s2) edge[] node[right] {$\sfrac{1}{2}$} (s6)
			 	(s5) edge[] node[right] {$\sfrac{1}{2}$} (roll_4)
			 	(s6) edge[] node[left] {$\sfrac{1}{2}$} (roll_5)
			 	(s6) edge[] node[right] {$\sfrac{1}{2}$} (roll_6)

				(roll_1) edge[loop below] node[below] {$1$} (roll_1)
				(roll_2) edge[loop below] node[below] {$1$} (roll_2)
				(roll_3) edge[loop below] node[below] {$1$} (roll_3)
				(roll_4) edge[loop below] node[below] {$1$} (roll_4)
				(roll_5) edge[loop below] node[below] {$1$} (roll_5)
				(roll_6) edge[loop below] node[below] {$1$} (roll_6)
				;
		\end{tikzpicture}
		\caption{A Markov chain for Knuth-Yao's six-sided die \cite{landscape}}
		\label{fig:die_mc}
	\end{center}
\end{figure}

\subsection{Markov chains and parametric Markov chains}

A (discrete-time) Markov chain \cite{handbook,parameter-synthesis,landscape} is a simple probabilistic model.
It consists of a set of states which are connected with transitions. These transitions are labeled with probabilities.
The sum of all outgoing probabilities of a state is one.
States in a Markov chain can be labeled with \emph{atomic propositions}, of which a given state can have multiple. 

\begin{definition}[Markov chain]\label{def:mc}
	A \emph{Markov chain (MC)} is a tuple \(\MC = (S, s_0, \Prob, AP, L)\) with
	\begin{itemize}
		\item a finite set \(S\) of states,
		\item an initial state \(s_0 \in S\),
		\item a transition function
		\(\Prob: S \times S \rightarrow [0, 1]\) such that
		\(\sum_{s' \in S} \Prob(s, s') = 1\) for all \(s \in S\),
		\item a finite set \(AP\) of atomic propositions, 
		\item and a labeling function \(L: S \rightarrow 2^{AP}\).
	\end{itemize}
\end{definition}

\begin{example}
	\cref{fig:die_mc} shows an MC that models
	a six-sided die using a series of fair coin tosses. From \(s_0\), the probability to flip heads and reach \(s_1\) or
	to flip tails and reach \(s_2\) is \(\Prob(s_0, s_1) = \Prob(s_0, s_2) = \sfrac{1}{2}\).
	In this example and in most following examples we do not show the set of atomic propositions
	or the labeling function corresponding to the MC.
\end{example}

\emph{Parametric Markov chains (pMCs)} are similar to MCs, but the transition probabilities are
polynomials. These polynomials can have multiple parameters and are defined over the real numbers.

\begin{definition}[Parametric Markov chain]
	\sloppypar
	A \emph{parametric Markov chain (pMC)} is a tuple
	\(\MC = (S, V, s_0, \Prob, AP, L)\) 
	with \(S, s_0, AP, \) and \(L\) as defined in \cref{def:mc} and
	\begin{itemize}
		\item a finite set \(V\) of parameters,
		\item and a transition function \(\Prob: S \times S \rightarrow \mathbb{R}[V]\).
	\end{itemize}
	\cite[see][6]{parameter-synthesis}, \cite[also see][3]{monotonic}.
\end{definition}

\begin{figure}
	\begin{center}
		\begin{tikzpicture}
			\node[state, initial] (s0) {$s_0$};
			\node[state, right of=s0] (s1) {$s_1$};
			\node[state, above of=s1] (s2) {$s_2$};
			
			\draw 	(s0) edge[bend left] node[auto] {$p$} (s1)
				(s1) edge[bend left] node[auto] {$q$} (s0)
				(s1) edge[loop right] node[auto] {$1-q$} (s1)
				(s2) edge[loop right] node[auto] {$p^2+q^2$} (s2)
				(s2) edge node[auto] {$1-(p^2+q^2)$} (s1)
				(s0) edge[bend left] node[auto] {$1-p$} (s2);
		\end{tikzpicture}
	\end{center}
	\begin{adjustbox}{width=\linewidth,center}
	\begin{subfigure}[b]{0.3\linewidth}
		\begin{center}
		\scalebox{0.8}{
		\begin{tikzpicture}
			\node[state, initial] (s0) {$s_0$};
			\node[state, right of=s0] (s1) {$s_1$};
			\node[state, above of=s1] (s2) {$s_2$};
			
			\draw 	(s0) edge[bend left] node[auto] {$0.5$} (s1)
				(s1) edge[bend left] node[auto] {$0.1$} (s0)
				(s1) edge[loop right] node[auto] {$0.9$} (s1)
				(s2) edge[loop right] node[auto] {$0.26$} (s2)
				(s2) edge node[auto] {$0.74$} (s1)
				(s0) edge[bend left] node[auto] {$0.5$} (s2);
		\end{tikzpicture}
		}
		\end{center}
		\caption{Instantiation \(a\)}
		\label{fig:instantiated_mc}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\linewidth}
		\begin{center}
		\scalebox{0.8}{
		\begin{tikzpicture}
			\node[state, initial] (s0) {$s_0$};
			\node[state, right of=s0] (s1) {$s_1$};
			\node[state, above of=s1] (s2) {$s_2$};
			
			\draw 	(s0) edge[bend left] node[auto] {$0.9$} (s1)
				(s1) edge[bend left] node[auto] {$0.9$} (s0)
				(s1) edge[loop right] node[auto] {$0.1$} (s1)
				(s2) edge[loop right] node[auto] {$1.62$} (s2)
				(s2) edge node[auto] {$-0.62$} (s1)
				(s0) edge[bend left] node[auto] {$0.1$} (s2);
		\end{tikzpicture}
		}
		\end{center}
		\caption{Instantiation \(b\)}
		\label{fig:instantiated_mc_not_well_defined}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\linewidth}
		\begin{center}
		\scalebox{0.8}{
		\begin{tikzpicture}
			\node[state, initial] (s0) {$s_0$};
			\node[state, right of=s0] (s1) {$s_1$};
			\node[state, above of=s1] (s2) {$s_2$};
			
			\draw 	(s0) edge[bend left] node[auto] {$0$} (s1)
				(s1) edge[bend left] node[auto] {$0.5$} (s0)
				(s1) edge[loop right] node[auto] {$0.5$} (s1)
				(s2) edge[loop right] node[auto] {$0.25$} (s2)
				(s2) edge node[auto] {$0.75$} (s1)
				(s0) edge[bend left] node[auto] {$1$} (s2);
		\end{tikzpicture}
		}
		\end{center}
		\caption{Instantiation \(c\)} 
		\label{fig:instantiated_mc_not_graph_preserving}
	\end{subfigure}
	\end{adjustbox}
	\caption{A parametric Markov chain and three of its instantiations} 
	\label{fig:first_pmc}
\end{figure}


\begin{example}\label{example:first_pmc}
	\cref{fig:first_pmc} shows a pMC with parameters \(V = \{p, q\}\).
\end{example}

Given a pMC, we can replace parameters with concrete values.
A map from parameters in \(V\) to values in \(\mathbb{R}\) is an \emph{instantiation} of the pMC.

\begin{definition}[Instantiated pMC]
	For a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\) and an instantiation
	\(u: V \rightarrow \mathbb{R}\), the
	\emph{instantiated pMC at \(u\)} is given by \(\MC[u] = (S, s_0, \Prob[u], AP, L)\)
	with \(\Prob[u](s, s') = \Prob(s, s')[u]\) for all \(s, s' \in S\).
	\parencite[see][7]{parameter-synthesis}.
\end{definition}

Not all instatiations of a pMC \emph{make sense}. We want the instantiated pMC to have the property
that all outgoing probabilities of every state sum up to one, and every probability is somewhere in
\([0, 1]\).  An instantiated pMC that fulfills these requirements is an MC, and an instantiation
that yields an MC is called \emph{well-defined}.
Thus, an instantiation \(u\) is well-defined for a pMC \(\MC\) if \(\MC[u]\) is an MC.
\parencite[see][8]{parameter-synthesis}.

\begin{example}
	Consider the pMC in \cref{fig:first_pmc}. With the instantiation \(a: p \mapsto 0.5, q \mapsto 0.1\),
	we get the instantiated pMC in \cref{fig:instantiated_mc}.
	We see that \(\sum_{s' \in S} \Prob(s, s') = 1\) for all \(s \in S\)
	and \(\Prob: S \times S \rightarrow [0, 1]\), so this instantiated pMC is also an MC.
\end{example}

\begin{example}
	Consider \cref{fig:first_pmc} and the instantiation \(b: p \mapsto 0.9, q \mapsto 0.9\).
	The instantiated pMC in \cref{fig:instantiated_mc_not_well_defined} is not an MC,
	as the outgoing transitions of \(s_2\) are not valid probabilities. Thus, the instantiation \(b\)
	is not well-defined.
\end{example}

It is possible that well-defined instantiations do not preserve the graph structure of a pMC.
This happens when the probability of a transition is zero in the instantiated pMC but not
zero in the uninstantiated pMC. Such an instantiation is not \emph{graph-preserving}.
More precisely, a well-defined instantiation \(u\) for a pMC \(\MC\) is graph-preserving if for
all \(s, s' \in S\): \(
	\Prob(s, s') \neq 0 \implies \Prob(s, s')[u] \neq 0.
\)
\parencite[see][8]{parameter-synthesis}.

\begin{example}
	Consider \cref{fig:first_pmc} and the instantiation \(c: p \mapsto 0, q \mapsto 0.5\).
	The resulting pMC in \cref{fig:instantiated_mc_not_graph_preserving} is an MC, so \(c\) is well-defined.
	But \(c\) is not graph-preserving, as \(\Prob(s_0, s_1) \neq 0\) in \cref{fig:first_pmc},
	but \(\Prob(s_0, s_1) = 0\) in \cref{fig:instantiated_mc_not_graph_preserving}.
	We might as well have not drawn an arrow from \(s_0\) to
	\(s_1\)---instantiating the pMC \cref{fig:first_pmc} with \(c\) removes that transition. 
\end{example}

The transition function of a pMC \(\MC\) can be expressed as a matrix, the \emph{transition matrix} of \(\MC\).
% If \(\MC\) is an MC, this matrix is stochastic.

\begin{definition}[Transition matrix] \label{def:transition_matrix}
	Given a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\) with states\\
	\(S = \{s_0, s_1, \ldots, s_{n-1}\}\), the transition matrix of \(\MC\) is the matrix
	\[
		M_{\MC} = \begin{pmatrix}
			\Prob(s_0, s_0) & \Prob(s_0, s_1) & \cdots & \Prob(s_0, s_{n-1}) \\
			\Prob(s_1, s_0) & \Prob(s_1, s_1) & \cdots & \Prob(s_1, s_{n-1}) \\
			\vdots & \vdots & \ddots & \vdots \\
			\Prob(s_{n-1}, s_0) & \Prob(s_{n-1}, s_1) & \ldots & \Prob(s_{n-1}, s_{n-1})
		\end{pmatrix}.
	\]
\end{definition}

A \emph{path} in a pMC \(\MC\) starts at the intitial state \(s_0\)
and visits states in some order, always following transitions that are not zero.
The paths we consider are always infinitely long.

\begin{definition}[Paths] \label{def:paths}
	Given a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\), \(\Paths(\MC) \subseteq S^\omega\) denotes the set of (infinite) paths
	\(s_0 s_1 \ldots\) in \(\MC\) where \(\Prob(s_i, s_{i+1}) \neq 0\) for \(i \geq 0\).
	Here, \(A^\omega\) for a set \(A\) denotes the set of all infinite sequences of elements in \(A\).
	\cite[7]{parameter-synthesis} \cite[752]{baier-katoen}
\end{definition}

Sometimes we are not interested in the states that a given path visits, but only in the labels of
these states. For this, we assign a \emph{trace} to every path in a pMC \(\MC\), which is not an
infinite sequence of states but an infinite sequence of the sets of labels that the visited states
have.

\begin{definition}[Traces]
	Given a path \(\pi = s_0 s_1 \ldots\) in a pMC \(\MC\), the \emph{trace} of \(\pi\)
	is defined as \(\text{trace}(\pi) = L(s_0) L(s_1) \ldots \in (2^{AP})^\omega\).
	The traces of a pMC \(\MC\) are \(\text{Traces}(\MC) = \{\text{trace}(\pi) \mid \pi
	\in \Paths(\MC)\}\).
\end{definition}

\subsection{LTL syntax and semantics}

LTL is a temporal logic that reasons about traces in transition systems with labels (like pMCs).  An
LTL formula operates on a trace \(A_0 A_1 A_2 \ldots \in (2^{AP})^\omega\).  A label \(a \in AP\)
forms an LTL formula that holds if the current state has the label \(a\), which means that \(a \in
A_0\). The LTL formula \(\bigcirc \phi\) holds if \(\phi\) holds in the next state,
so if \(A_1 A_2 A_3 \ldots\) satisfies \(\phi\).
The LTL formula \(\phi_1 \LTLUntil \phi_2\) holds if \(\phi_2\) is true now or will
be true sometime in the future, and \(\phi_1\) holds until then. LTL formulae can also be composed
and negated like propositional logic formulae.

\begin{definition}[Syntax of LTL]
	LTL formulae over the set \(AP\) of atomic propositions are formed according to the following grammar:
	\[
		\phi := \text{true} \mid a \mid \phi_1 \land \phi_2 \mid \lnot \phi \mid \bigcirc \phi \mid \phi_1 \LTLUntil \phi_2
	\]
	where \(a \in AP\).
	\cite[231]{baier-katoen}
\end{definition}

\begin{definition}[Semantics of LTL]
	Given a trace \(\sigma = A_0 A_1 A_2 \ldots \in (2^{AP})^\omega\) and an LTL formula \(\phi\), the satisfaction
	relation \(\vDash\) for LTL is defined as follows \cite[235]{baier-katoen}:
	\begin{alignat*}{2}
		\sigma &\vDash \text{true} && \\
		\sigma &\vDash a \in AP &&\text{ iff } a \in A_0 \\
		\sigma &\vDash \phi_1 \land \phi_2 \qquad &&\text{ iff } \sigma \vDash \phi_1 \text{ and } \sigma \vDash \phi_2 \\
		\sigma &\vDash \lnot \phi &&\text{ iff not } \sigma \vDash \phi \\
		\sigma &\vDash \bigcirc \phi &&\text{ iff } \sigma[1\ldots] \vDash \phi \\
		\sigma &\vDash \phi_1 \LTLUntil \phi_2 &&\text{ iff } \exists j \geq 0: \sigma[j\ldots] \vDash \phi_2
		\text{ and } \forall 0 \leq i < j: \sigma[i\ldots] \vDash \phi_1
	\end{alignat*}
	where \(\phi[i\ldots] = A_i A_{i+1} A_{i+2} \ldots \in (2^{AP})^{\omega}\).
\end{definition}

\begin{definition}[Probability of LT Properties]
	Let \(\MC\) be a pMC with atomic propositions \(AP\) and a state \(s \in S\) and \(\phi\) be
	an LTL formula. We then write \(\Pr_{\MC}(s \vDash \phi)\) for the probability that the
	trace of a path in \(\MC\) starting in \(s\) satisfies \(\phi\).
	\cite[see][796]{baier-katoen}
\end{definition}

\subsection{Eventually formulae and reachability}

We are interested in the probability of eventually reaching some set of states in a pMC.
This can be expressed in LTL using the \emph{diamond operator} \(\diamondsuit \phi := \text{true}
\LTLUntil \phi\).  This formula holds if \(\phi\) holds at some point.

\begin{figure}
	\begin{center}
		\begin{tikzpicture}
			\node[state, initial] (s0) {$s_0$};
			\node[state, right of=s0] (s1) {$s_1$};
			\node[state, right of=s1] (s2) {$\good$};
			\node[state, above of=s1] (s3) {$\bad$};
			
			\draw 	(s0) edge node[auto] {$p$} (s1)
				(s0) edge node[auto] {$1-p$} (s3)
				(s1) edge node[auto] {$1-p$} (s2)
				(s1) edge[right] node{$p$} (s3)
				(s2) edge[loop right] node[auto] {$1$} (s2)
				(s3) edge[loop right] node[auto] {$1$} (s3);
		\end{tikzpicture}
	\end{center}
	\caption{A pMC with a maximum of the reachability function at \(p=0.5\).}\label{fig:gradient_construction_mc}
\end{figure}

\begin{example}
	Consider the MC in \cref{fig:die_mc} and assume the state \(\epsdice{5}\) is labeled with the
	atomic proposition \(\epsdice{5} \in AP\). Then, \(\Pr(s_0 \vDash \diamondsuit \epsdice{5}) =
	\frac{1}{6}\).
\end{example}
\begin{example}
	Consider the pMC in \cref{fig:gradient_construction_mc} and assume the state \(\good\) is
	labeled with the atomic proposition \(\good \in AP\). Then, \(\Pr(s_0 \vDash \diamondsuit \good) = p
	\cdot (1-p)\).
\end{example}

We use the diamond operator to define \emph{reachability specifications}. A reachability
specification is written \(\Prob_J(\diamondsuit \good)\) and is satisfied if the probability
of reaching a state labeled with \(\good\) fulfills some bound \(J\).

\begin{definition}[Reachability specification]
	Given an MC \(\MC\) with an initial state \(s_0\), the \emph{reachability specification}
	\(\varphi_r = \Prob_J(\diamondsuit \good)\) for \(\good \in AP\) and \(J \subseteq [0, 1]\)
	is satisfied by \(\MC\) if \(\Pr_{\MC}(s_0 \vDash \diamondsuit \good) \in J\).
	This is written \(\MC \vDash \varphi_r\).
\end{definition}

\begin{remark}
	In reachability specifications, the set \(J\) is often abbreviated. For instance, if \(J = [0, 0.5]\),
	we might write \(\Prob_{\leq 0.5}(\diamondsuit \good)\).
\end{remark}

\subsection{Conventions and assumptions}

In this thesis, we mainly use the LTL formula \(\diamondsuit \good\) for an atomic proposition in
\(\good \in AP\).
Throughout the remaining chapters, we use the following conventions in regard to the states and
labels \(\good\) and \(\bad\).
If a state is called \(\good\), this indicates that this state is the only state labeled with
\(\good \in AP\) and the only state \(s\) where \(\Pr(s \vDash \diamondsuit \good) = 1\).
We call this state the target.  If a state is called \(\bad\), this indicates
that this state is the only state \(s\) from which \(\good\) is not reachable, so the only state where
\(\Pr(s \vDash \diamondsuit \good) = 0\).  If not otherwise noted, we from now on assume all pMCs to
have exactly one state called \(\good\) and one state called \(\bad\) with the above properties.
Any pMC that has multiple states labeled with \(\good\) or multiple states from which \(\good\) is
not reachable can be converted into a pMC that fulfills our assumptions with standard preprocessing
techniques while preserving all properties regarding the eventually formula \(\diamondsuit \good\)
\cite{baier-katoen}. Both \(\good\) and \(\bad\) are absorbing, meaning that their only outgoing
transition is a self-loop with probability one.

We assume that for all considered pMCs with parameters \(V\), all instantiations \(u: V \rightarrow
[0,1]\) are well-defined and all instantiations \(u: V \rightarrow (0,1)\) are graph-preserving.
