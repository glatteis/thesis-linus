\section{Computing the reachability function}\label{section:exact}

In this section, we show a way to compute the reachability function given a pMC and a
target state. This method is then extended in \cref{section:construction} to compute the
derivative of the reachability function. Using this extended method, we can then perform
Gradient Descent on the pMC.

To retrieve the probability that the target \(\good\) in a pMC \(\MC\) with states \(S\) will
eventually be reached, one can compute the reachability function for every state in \(S\) using a
linear system of equations.  A variable \(x_s\) is introduced for every state \(s \in S\) for the
probability to eventually reach \(\good\) from \(s\).  We already know that the probability to reach
\(\good\) from \(\good\) is one, so we set \(x_{\good} = 1\). We also know that the probability to
reach \(\good\) from \(\bad\) is zero, so we set \(x_{\bad} = 0\).  Then for any other state \(s \in
S\), the probability to reach the target from \(s\) can be computed as follows: One takes a
successor state \(s'\) of \(s\) and considers the probability \(s'\) eventually reaches the target,
which is \(x_{s'}\).  The probability to reach the target from \(s\) with \(s'\) as the next state
is then \(\Prob(s, s') \cdot x_{s'}\).  The probability to eventually reach the target from \(s\) is
the sum of this product over all successor states \(s'\) of \(s\).

\newcommand\systemofequationsdef{
	\begin{align*}
		x_{\bad} &= 0 & \\
		x_{\good} &= 1 & \\
		x_s &= \sum_{s' \in S} \Prob(s, s') \cdot x_{s'} &\text{for } s \in S \setminus \{\good, \bad\}
	\end{align*}
}
\begin{definition}\label{def:system_of_equations}
	Given a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\),
	the probability to eventually reach \(\good\) from any state \(s \in S\) can be computed using
	the system of equations
	\systemofequationsdef
	\cite[760]{baier-katoen}
\end{definition}

Solving this system of equations yields the exact reachability function for every state.  We cannot
generally solve this in \(\mathbb{R}[V]\), but we can solve it in the rational functions over \(V\),
\(\mathbb{R}(V)\) \cite{probabilistic_reachability}.  The reachability function of a pMC is thus
generally not a polynomial over \(V\), but a rational function. 
Its worst-case length is exponential in the number of parameters but polynomial in the number of
states of the pMC \cite[124]{sebastiandiss}.

Given a pMC \(\MC\) and an instantiation \(u: V \rightarrow \mathbb{R}\), a reachability probability
in \(\MC[u]\) can also be computed by constructing this system of equations directly on \(\MC[u]\).
It is then a system of equations in \(\mathbb{R}\).

\begin{figure}
	\begin{center}
		\begin{tikzpicture}
			\node[state, initial] (s0) {$s_0$};
			\node[state, right of=s0] (s1) {$s_1$};
			\node[state, right of=s1] (s2) {$\good$};
			\node[state, above of=s1] (s3) {$\bad$};
			
			\draw 	(s0) edge node[auto] {$p$} (s1)
				(s0) edge node[auto] {$1-p$} (s3)
				(s1) edge node[auto] {$q$} (s2)
				(s1) edge[right] node{$1-q$} (s3)
				(s2) edge[loop right] node[auto] {$1$} (s2)
				(s3) edge[loop right] node[auto] {$1$} (s3);
		\end{tikzpicture}
	\end{center}
	\caption{An example pMC}\label{figure:exact_pmc}
\end{figure}

\begin{example}\label{example:exact_1}
	Consider the pMC in \cref{figure:exact_pmc} with parameters \(V = \{p, q\}\).
	We compute the reachability function.
	This yields the following system of equations:
	\begin{align*}
		x_{\bad} &= 0 \\
		x_{\good} &= 1 \\
		x_0 &= p \cdot x_1 + (1-p) \cdot x_{\bad} \\
		x_1 &= q \cdot x_{\good} + (1-q) \cdot x_{\bad}
	\end{align*}

	As the equation system is already in upper triangular form, we can immediately solve it and
	get the rational functions \(x_0 = p \cdot q\), \(x_1 = q\), \(x_{\good} = 1\) and \(x_{\bad} = 0\).
	We see that the reachability function is \(\Pr(s_0 \vDash \diamondsuit \good) = p \cdot q\).
\end{example}

We now convert the system of equations in \cref{def:system_of_equations} into the form \(Ax = b\)
for a matrix \(A\) and vectors \(x\) and \(b\).

\begingroup\allowdisplaybreaks
\begin{theorem}\label{theorem:equiv_system_exact}
	\sloppypar
	Let a pMC \(\MC = (S, V, s_0, \Prob, AP, L)\) and a target \(\good\) be given.  The system
	of equations in \cref{def:system_of_equations} has the same solutions for \(\widetilde{S} =
	S \setminus \{\good, \bad\} = \{s_0, s_1, \ldots, s_{n-1}\}\) as the linear system of
	equations
	\begin{align*}
		&(1 - A)
		\begin{pmatrix}
			x_0 \\ x_1 \\ \vdots \\ x_{n-1}
		\end{pmatrix}
		=
		\begin{pmatrix}
			\Prob(s_0, \good) \\ 
			\Prob(s_1, \good) \\ 
			\vdots \\
			\Prob(s_{n-1}, \good) \\ 
		\end{pmatrix}
	\end{align*}
	where the variable for \(s_i\) is called \(x_i\) and  \(A\) is the constrained transition
	matrix for \(\MC\) in which the rows and columns for \(\good\) and \(\bad\) are omitted.

	\begin{proof}
		Remember \cref{def:system_of_equations}:
		\systemofequationsdef
		We are not interested in \(\bad\) as \(\Pr(\bad \vDash \diamondsuit \good) = 0\),
		so we replace all occurences of \(x_{\bad}\) with \(0\).
		In the same way, we can replace all occurences of \(x_{\good}\) with \(1\).
		This yields constant probabilities which we can add seperately (see below).
		Let \(\widetilde{S} = S \setminus \{\good, \bad\} = \{s_0, s_1, \ldots, s_{n-1}\}\),
		then \cref{def:system_of_equations} has the same solutions for states in
		\(\widetilde{S}\) as
		\begin{align*}
			\begin{pmatrix}
				x_0 \\ x_1 \\ \vdots \\ x_{n-1}
			\end{pmatrix}
			=& 
			\begin{pmatrix}
				\sum_{i = 0}^{n-1} \Prob(s_0, s_i) x_i \\
				\sum_{i = 0}^{n-1} \Prob(s_1, s_i) x_i \\
				\vdots \\
				\sum_{i = 0}^{n-1} \Prob(s_{n-1}, s_i) x_i
			\end{pmatrix}
			+
			\begin{pmatrix}
				\Prob(s_0, \good) \\ 
				\Prob(s_1, \good) \\ 
				\vdots \\
				\Prob(s_{n-1}, \good) \\ 
			\end{pmatrix}
		\end{align*}
		We observe the middle vector is equal to \(A (x_0, x_1, \ldots, x_{n-1})^T\)
		for the constrained transition matrix \(A\) for the states \(\widetilde{S}\) (see
		\cref{def:transition_matrix}).
		Thus let \[
			A = \begin{pmatrix}
				\Prob(s_0, s_0) & \Prob(s_0, s_1) & \cdots & \Prob(s_0, s_{n-1}) \\
				\Prob(s_1, s_0) & \Prob(s_1, s_1) & \cdots & \Prob(s_1, s_{n-1}) \\
				\vdots & \vdots & \ddots & \vdots \\
				\Prob(s_{n-1}, s_0) & \Prob(s_{n-1}, s_1) & \ldots & \Prob(s_{n-1}, s_{n-1})
			\end{pmatrix},
		\] then we can rewrite the above equation as
		\begin{align*}
			\begin{pmatrix}
				x_0 \\ x_1 \\ \vdots \\ x_{n-1}
			\end{pmatrix}
			&=
			A
			\begin{pmatrix}
				x_0 \\ x_1 \\ \vdots \\ x_{n-1}
			\end{pmatrix}
			+
			\begin{pmatrix}
			\Prob(s_0, \good) \\ 
			\Prob(s_1, \good) \\ 
			\vdots \\
			\Prob(s_{n-1}, \good) \\ 
			\end{pmatrix}
			\\
			\Leftrightarrow
			(1 - A)
			\begin{pmatrix}
				x_0 \\ x_1 \\ \vdots \\ x_{n-1}
			\end{pmatrix}
			&=
			\begin{pmatrix}
			\Prob(s_0, \good) \\ 
			\Prob(s_1, \good) \\ 
			\vdots \\
			\Prob(s_{n-1}, \good) \\ 
			\end{pmatrix}.
			\end{align*}
		This is the linear system of equations in \cref{theorem:equiv_system_exact}.
	\end{proof}
\end{theorem}
\endgroup

\begin{example}
	The linear system of equations of \cref{example:exact_1} constructed after
	\cref{theorem:equiv_system_exact} is
	\[
		\begin{pmatrix}
			1 & -p \\
			0 & 1 \\
		\end{pmatrix}
		\begin{pmatrix}
			x_0 \\ x_1
		\end{pmatrix}
		= \begin{pmatrix}
			0 \\ q
		\end{pmatrix}
	\]
	Solving it gives the vector \((p \cdot q, q)^T\).  The first row represents \(s_0\) and the
	second row represents \(s_1\), as these are the two states that are not \(\good\) or
	\(\bad\).
	We conclude that \(\Pr(s_0 \vDash \diamondsuit \good) = p \cdot q\) and \(\Pr(s_1 \vDash
	\diamondsuit \good) = q\).
\end{example}

\begin{theorem}\label{theorem:unique_solution_book}
	Given a well-defined and graph-preserving instantiation \(u\) of the pMC \(\MC\), the system
	of equations in \cref{def:system_of_equations} has the unique solution \(x_i = \Pr(s_i \vDash
	\diamondsuit \good)\) for all \(s_i\) in \(\widetilde{S}\) in the MC \(\MC[u]\).

	\begin{proof}
		This is proved in \cite[766]{baier-katoen}.
	\end{proof}
\end{theorem}

