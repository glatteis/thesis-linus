\section{Evaluation}

We are benchmarking the two types of problems described in \cref{section:problem}.  We are comparing
the performance of our method to the performance of a Parameter Lifting based approach already
present in \texttt{storm}.  This method performs the Parameter Lifting based divide-and-conquer
approach described in \cref{section:parameter_lifting} normal Parameter Lifting and terminates if
either the bound is reached (when solving the feasible instantiation problem) or if it cannot find
any regions that have greater reachability probabilities than already known regions (when solving
the pMC extremum problem).

\subsection{Case studies}

We consider both models expressed as pMCs directly in \cref{subsubsection:pmc},
and pMCs synthesized from \emph{POMDPs} in \cref{subsubsection:pomdp}.

\subsubsection{pMC Case studies}\label{subsubsection:pmc}

The pMC case studies can all be found on the PARAM Tool Homepage \cite{param} or on the PRISM Tool
Homepage \cite{prism}.  We chose these specific models because they are all MCs, have non-trivial
reachability probabilities and a large number of states.

\begin{description}
	\item[nand]
		The \texttt{nand} MC performs \emph{NAND multiplexing} \cite{nand}. Multiplexing is a technique for constructing
		approximately reliable computations from unreliable devices. Instead of having only one copy of the device,
		there are \(N\) copies which operate simultaneously on the same input. If the devices and inputs are reliable,
		all copies will produce the same output, but when they are unreliable the outputs can be different.
		In multiplexing, a threshold is defined from which the output will be accepted as decided if a certain number
		of devices agree with each other, and otherwise the output will be undecided. 
		The executive stage, which performs the calculations, is appended by a restorative stage,
		which decides if the output was certain or not.
		In the \texttt{nand} model, the original gate that is modelled using multiplexing is a NAND gate.
		\begin{description}
			\item[Parameters: ] \hfill
				\begin{itemize}
					\item \(p_{err}\): the probability that a gate has an error.
					\item \(p_{in}\): the probability that an input is stimulated.
				\end{itemize}
			\item[Specification: ] What is the probability that the system has a correct output?
			\item[Constants: ] \hfill
				\begin{itemize} 
					\item \(N\): the number of inputs in each bundle. We set
						\(N=20\).
					\item \(K\): the number of restorative stages. We set
						\(K=3\).
				\end{itemize}
			\item[Number of states: ] 231552 original, 76980 after minimization
			\item[Number of transitions: ] 358152 original, 272795 after minimization
		\end{description}
	\item[herman]
		The \texttt{herman} MC is a model of \emph{Herman's self-stabilizing algorithm} \cite{herman}.
		This algorithm starts with a ring of \(N\) identical processes, where \(N\) is odd.
		Processes can own \emph{tokens}. The goal is that eventually only one process has a token
		(this is called a \emph{stable} configuration).
		The algorithm starts out with a random distribution of tokens.
		It operates as follows: Every time step, each process with a token throws a biased coin with probability \(p\).
		If the coin shows heads, the process passes its token to the next process in a common direction.
		Else, the process keeps the token. When two tokens are held by the same process, they are both eliminated.
		When the protocol starts with an odd number of tokens, it will thus remain at an odd number.
		\begin{description}
			\item[Parameters: ] \hfill
				\begin{itemize}
					\item \(p\): the probability that a token is passed on.
				\end{itemize}
			\item[Specification: ] What is the probability that the system is eventually stable
				after a maximum of \(MAX\) steps?
			\item[Constants: ] \hfill
				\begin{itemize}
					\item \(N\): the number of processes. We set \(N=11\).
					\item \(MAX\): the maximum number of steps. We set
						\(MAX=100\).
				\end{itemize}
			\item[Number of states: ] 205820 original, 201589 after minimization
			\item[Number of transitions: ] 2524966 original, 2517542 after minimization
		\end{description}
	\item[crowds] The \texttt{crowds} MC \cite{crowds} is a model of the \emph{Crowds system for anonymous web
		browsing}. This is a gossip-based system where anonymity is achieved by routing
		messages randomly among a group of users. Whenever a malicous user recieves a
		message, they cannot be sure whether the message was composed by the sender or if the
		sender was simply forwarding it. But it is possible that this anonymity is degraded.
		If there are multiple malicious users in the
		system that are sharing information, the probability that they will be able to detect
		the sender of a given message increases.
		\begin{description}
			\item[Parameters: ] \hfill
				\begin{itemize}
					\item \(PF\): the probability that a message is forwarded.
					\item \(badC\): the probability that a member is
						untrustworthy.
				\end{itemize}
			\item[Specification: ] What is the probability that the malicious
				user(s) observed the real sender and only the real sender more than
				once?
			\item[Constants: ] \hfill
				\begin{itemize}
					\item \(TotalRuns\): the number of different paths to be
						analyzed. We set \(TotalRuns = 10\).
					\item \(CrowdSize\): the actual number of honest members in the
						crowd. We set \(CrowdSize = 4\).
					\item \(MaxGood\): the maximum number of honest members in the
						crowd. We set \(MaxGood = 20\).
				\end{itemize}
			\item[Number of states: ] 52973 original, 2323 after minimization
			\item[Number of transitions: ] 86133 original, 8123 after minimization
		\end{description}
	\item[brp] The \texttt{brp} MC \cite{brp} is a model of the \emph{Bounded retransmission
		protocol}. A sender uses the protocol to send data to a reciever, split up into frames.
		Incoming frames are checked for errors by the reciever and have to be acknowleged
		such that the sender sends a new frame. When no acknowlegement is recieved, the
		sender resends the frame. This is repeated until the sender gives up after \(MAX\)
		transmissions.
		\begin{description}
			\item[Parameters: ] \hfill
				\begin{itemize}
					\item \(pL\) and \(pK\): the reliabilities of the two
					channels (sender-to-reciever and reciever-to-sender).
				\end{itemize}
			\item[Specification: ] 
				What is the 
				the probability the sender reports an unsuccessful transmission
				after more than 8 chunks have been sent successfully?
			\item[Constants: ] \hfill
				\begin{itemize}
					\item \(N\): the number of chunks. We set \(N=10\).
					\item \(MAX\): the maximum number of retransmissions.
						We set \(MAX=2048\).
				\end{itemize}
			\item[Number of states: ] 268447 original, 90133 after minimization
			\item[Number of transitions: ] 368819 original, 180263 after minimization
		\end{description}
	\item[zeroconf] The \texttt{zeroconf} MC \cite{zeroconf} is a model of the \emph{Zeroconf protocol for
		automatic network configuration}. When a new device logs into a network, the
		Zeroconf protocol can be used by the host to assign the device an IP address
		which is unique within the network. The host selects a random IP address \(U\) from
		the set of possible local IP addresses and broadcasts a message asking if any
		existing device is already using \(U\). If the host recieves a reply from a device
		that is already using \(U\), the host selects a new address and repeats the process.
		It is possible that a device is already using \(U\) but the message or its reply
		gets lost.
		If the host recieves no reply after sending \(n\) messages, it assigns the new
		device the IP address \(U\) and the Zeroconf protocol is finished.
		This means it is also possible that an IP address that is already used in the
		network is be assigned to the new device, which can lead to undesirable outcomes.
		\begin{description}
			\item[Parameters: ]\hfill
				\begin{itemize}
					\item \(pL\): the probability that the host chooses an IP
						address that is already used
					\item \(pK\): the probability that a message gets lost
				\end{itemize}
			\item[Specification: ] What is the probability that an IP address that is
				already used is assigned to a new device?
			\item[Constants: ] \hfill
				\begin{itemize}
					\item \(n\): the number of attempts to recieve a reply. We
						set \(n=102400\).
				\end{itemize}
			\item[Number of states: ] 102404 original, 102403 after minimization
			\item[Number of transitions: ] 204805 original, 204804 after minimization
		\end{description}
\end{description}

\subsubsection{POMDP case studies}\label{subsubsection:pomdp}

A \emph{Partially Observable Markov Decision Process (POMDP)} is a more powerful probabilistic model than the models
we have discussed in this thesis.
A POMDP is a generalization of a \emph{Markov Decision Process (MDP)}, which is in turn a generalization of a Markov chain.
In Markov chains, transition probabilities from certain states are always the same. In MDPs, there can be
multiple sets of transition probabilities. Which set of probabilities is actually used is chosen by
an \emph{agent} operating on the MDP.
In MDPs, agents act with full knowledge of the model's state. The function from state to a
distribution over actions is called their \emph{(memoryless) strategy}.  In POMDPs, the agent only observes
the \enquote{color} of the current state. More formally, a POMDP contains a mapping from states to
colors. A memoryless strategy in a POMDP then is a function from such a color to a distribution over
actions.  The \emph{POMDP synthesis problem} now asks for a memoryless strategy of the agent that fulfills
some specification.

We consider POMDPs here because the POMDP Synthesis Problem turns out to
be polynomially reducible
to the feasible instantiation problem that our method addresses \cite{finite_state_contr}.
Given a reachability specification \(\varphi_r\), the POMDP can be converted to a pMC that has an instantiation satisfying
\(\varphi_r\) if and only if there exists a so-called \emph{finite state controller}
for the POMDP satisfying \(\varphi_r\), which produces a strategy for the agent.\footnote{
To read more about this, refer to \cite{finite_state_contr}.
}

\begin{description}
	\item[newgrid]
		The \texttt{newgrid} \cite[396]{pomdps} POMDP simulates a robot that is placed randomly
		in a \(n \times n\) grid. The robot can move in all four directions on the grid.
		Only the target in the south east corner looks different to the robot.
		From the synthesized parameters, one can compute a
		strategy for the robot to reach the target.
		\begin{description}
			\item[Parameters in constructed pMC: ] 3
			\item[Specification: ] Synthesize a strategy that maximizes the probability
				to eventually land in the target.
			\item[Constants: ] \hfill
				\begin{itemize}
					\item \(n\): the height and width of the grid. We set \(n=10\).
				\end{itemize}
			\item[Number of states in constructed pMC: ] 366 original, 360 after minimization
			\item[Number of transitions in constructed pMC: ] 728 original, 720 after minimization
		\end{description}
	\item[crypt]
		The \texttt{crypt} \cite[391]{pomdps} POMDPs are models of the \emph{dining cryptographers
		protocol}. A group of \(N\) cryptographers are sitting at a table.
		Either one of the cryptographers or the host (who is not sitting at the table) is paying for dinner.
		If one of the cryptographers paid, this person wants to stay anonymous.
		Using the dining cryptographers protocol, the cryptographers can find out whether one
		of them or the host paid for dinner without revealing which cryptographer has paid.
		The protocol involves a number of coin flips.

		We have two \texttt{crypt} case studies: \texttt{crypt4-max}, where the reachability
		function is maximized and \texttt{crypt4-min}, where the reachability
		function is minimized.
		\begin{description}
			\item[Parameters in constructed pMC: ] 496 (4 Cryptographers)
			\item[Specification: ] From the perspective of one of the cryptographers, 
				maximize or minimize the probability to guess correctly which cryptographer paid
				assuming the host never pays for dinner.
				This probability is always \(\frac{1}{N-1}\).
			\item[Constants: ] \hfill
				\begin{itemize}
					\item \(N\): the number of cryptographers. We set \(N=4\).
				\end{itemize}
			\item[Number of states in constructed pMC: ] 3316 original, 1795 after
				minimization
			\item[Number of transitions in constructed pMC: ] 6003 original, 3634 after
				minimization
		\end{description}
	\item[nrp]
		The \texttt{nrp} POMDP \cite[390]{pomdps} is a model of the \emph{non-repudiation protocol}.
		This protocol allows an originator \(O\) to send information to a recipient \(R\) without any
		party being able to deny having participated in the information transfer.
		In the beginning of the protocol, \(O\) randomly picks a secret number
		\(N \in [1, K]\) for a constant \(K\). If \(R\) is malicious and able to guess
		this number, it can deny having participated in the information transfer.
		The POMDP model assumes \(R\) behaves maliciously by stopping the transfer early.
		While \(R\) can also use different malicous strategies in the actual protocol, these
		cannot be modeled with a POMDP.
		\begin{description}
			\item[Parameters in constructed pMC: ] 20
			\item[Constants: ] \hfill
				\begin{itemize}
					\item \(K\): The maximum value of the secret number.
					We set \(K = 20\).
				\end{itemize}
			\item[Specification: ] 
				Assuming \(R\) behaves maliciously by stopping early, compute the
				probability that \(R\) can obtain the information from \(O\)
				while being able to deny having participated.
			\item[Number of states in constructed pMC: ] 671 original, 213 after
				minimization
			\item[Number of transitions in constructed pMC: ] 900 original, 442 after
				minimization
		\end{description}
\end{description}

\subsection{Benchmarks}

We benchmarked both methods on a Intel Xeon Platinum 8160 with 16 GB of RAM and a timeout of 1 hour
(for finding optima) or 15 minutes (for finding feasible instantiations).  We ran each benchmark
five times and then computed the average and the standard deviation of all runtimes and found
probabilities.  We used Storm v1.6.1, which loaded and minimized the models before the respective
methods were started. The time spent loading and minimizing the model was not counted, only the
execution time of Gradient Descent or the Parameter Lifting based method.

\paragraph{Abbreviations in the tables}
In the tables, GD stands for Gradient Descent and PL for the Parameter Lifting method. TO stands for
a timeout (> 1 hour for the pMC extremum problem and > 15 minutes for the feasible instantiation
problem) and MO for out of memory (> 16 GB).
\(\varnothing\) stands for the average and \(\sigma\) for the standard deviation of the runtimes.
ERR stands for error, and in all where it occurs the
Parameter Lifting based method attempted to create an \texttt{std::vector} larger than
\texttt{max\_size()}. This happens because the Parameter Lifting based method attempts to create a
list of \(2^{496}\) vertices when considering \texttt{crypt}'s 496-dimensional search space.  NF
stands for not found, which occurs when the Parameter Lifting based method concludes that a case
study does not have an instantiation that satisfies the bound when solving the feasible
instantiation problem. Note that the Gradient Descent method is not able to come to this conclusion
and times out after searching for 15 minutes if a feasible instantiation does not exist.
Under \enquote{Found Probability} is the found optimal reachability probability, not the
found instantiation.

\subsubsection{Benchmarking the pMC extremum problem}

The results of the benchmarks for finding optima in each of the case studies can be seen in
\cref{fig:benchmarks_extremum}. In all cases where the search succeeded, the found optima are given
in the right columns. Note that when both methods reach a result, they agree with each other on all
case studies except for \texttt{zeroconf}, which we will discuss later, and \texttt{herman}.  In the
\texttt{herman} model, Gradient Descent finds a slightly higher probability with a difference of
0.0003 to the Parameter Lifting based method at similar instantiations (\(p \mapsto 0.4580\) for GD vs. \(p
\mapsto 0.4688\) for PL). We have not found out why this occurs.  The Parameter Lifting based method
crashes on the \texttt{crypt}, \texttt{newgrid} and \texttt{nrp} case studies, which are the case
studies synthesized from POMDPs, while Gradient Descent sucessfully finds their optima.  The
Gradient Descent method does not find the optimum of the \texttt{zeroconf} case study, which has the
reachability probability 1. This is because it starts at the instantiation that maps all parameters to 0.5, where the
derivative is so small that it is evaluated to zero.  Gradient Descent finds the optimum faster on
\texttt{brp} and the Parameter Lifting based method finds the optimum faster on \texttt{crowds},
\texttt{herman}, and \texttt{nand}.

We have plotted three Gradient Descent runs searching for optima in
\cref{figure:gradient_descent_runs}. Included are the values of the parameters and the reachability
probability (bold arrow) for every step of the descent. If a step was undone, the previous
instantiation and probability is plotted.  One can see that Gradient Descent on \texttt{nand} is
fairly straightforward. In the \texttt{brp} plot, one can see the effect of step size adaptation
clearly.

\begin{figure}
	\begin{subfigure}{\linewidth}
	\includegraphics[width=\linewidth]{visualizations/live/newgrid/out.pdf}
	\caption{Gradient Descent on \texttt{newgrid}}
	\label{figure:gradient_descent_newgrid}
	\end{subfigure}
	\begin{subfigure}{\linewidth}
	\includegraphics[width=\linewidth]{visualizations/live/nand/out.pdf}
	\caption{Gradient Descent on \texttt{nand}}
	\label{figure:gradient_descent_nand}
	\end{subfigure}
	\begin{subfigure}{\linewidth}
	\includegraphics[width=\linewidth]{visualizations/live/brp/out.pdf}
	\caption{Gradient Descent on \texttt{brp}}
	\label{figure:gradient_descent_brp}
	\end{subfigure}
	\caption{Plots of Gradient Descent searching for optima of the reachability
	function}
	\label{figure:gradient_descent_runs}
\end{figure}

\begin{table}
\begin{adjustbox}{width=\textwidth}

% BEGIN COPYING HERE ==========
\begin{tabular}{llrrrrrr}
\toprule
Model & Spec & \multicolumn{4}{c}{Time (s)} & \multicolumn{2}{c}{Found Probability} \\
\cmidrule(r){3-6}
\cmidrule(r){7-8}
&& \multicolumn{2}{c}{GD} & \multicolumn{2}{c}{PL} &
\multicolumn{1}{c}{GD} & \multicolumn{1}{c}{PL} \\
\cmidrule(r){3-4}
\cmidrule(r){5-6}
&& \multicolumn{1}{c}{\(\varnothing\)} &
\multicolumn{1}{c}{\(\sigma\)} & \multicolumn{1}{c}{\(\varnothing\)} &
\multicolumn{1}{c}{\(\sigma\)} &&
\\
\midrule
\texttt{brp} & max & \(22.7732\)&\(0.1682\) & \(3326.9136\)&\(67.2121\) & \(0.0433\) & \(0.0433\) \\
\texttt{crowds} & max & \(0.5924\)&\(0.0038\) & \(0.0718\)&\(0.0007\) & \(1.0000\) & \(1.0000\) \\
\texttt{crypt4-max} & max & \(6.4972\)&\(0.0219\) & \multicolumn{2}{c}{ERR} & \(0.3333\) & \multicolumn{1}{c}{N/A} \\
\texttt{crypt4-min} & min & \(7.2044\)&\(0.0420\) & \multicolumn{2}{c}{ERR} & \(0.3333\) & \multicolumn{1}{c}{N/A} \\
\texttt{herman} & max & \(495.3980\)&\(3.0661\) & \(32.4682\)&\(0.0367\) & \(0.8330\) & \(0.8327\) \\
\texttt{nand} & max & \(3.7742\)&\(0.0152\) & \(2.0898\)&\(0.0259\) & \(1.0000\) & \(1.0000\) \\
\texttt{newgrid} & max & \(0.1238\)&\(0.0004\) & \multicolumn{2}{c}{MO} & \(1.0000\) & \multicolumn{1}{c}{N/A} \\
\texttt{nrp} & max & \(0.2364\)&\(0.0035\) & \multicolumn{2}{c}{MO} & \(0.0500\) & \multicolumn{1}{c}{N/A} \\
\texttt{zeroconf} & max & \(2.1104\)&\(0.0175\) & \(1.7256\)&\(0.0070\) & \(0.0000\) & \(1.0000\) \\
\bottomrule
\end{tabular}
% END COPYING HERE ============

\end{adjustbox}
\caption{Benchmarks finding an extremum of the reachability function}
\label{fig:benchmarks_extremum}
\end{table}

\subsubsection{Benchmarking the feasible instantiation problem}

The results of the benchmarks for finding feasible instantiations in each of the case studies can be
seen in \cref{fig:benchmarks_feasible}.  The case studies were run with maximizing or minimizing
reachability specifications and the bounds 0.01, 0.3, 0.7, and 0.99.  
Gradient Descent is able to find the maximum in \texttt{newgrid} that the Parameter Lifting based
method does not find and finds feasible instantiations mostly in the same order of magnitude of
time.  Because the derivative is evaluated to zero at most instantiations in the \texttt{zeroconf}
case study, Gradient Descent only finds the maximum slowly or times out when using standard
precision.  We fixed this by increasing the precision and using rational numbers inside the solver.

\subsection{Conclusion}

Gradient Descent clearly performs better on models with a high number of dimensions where the
Parameter Lifting based method becomes infeasible. Gradient Descent also performs better on the
other pMCs synthesized from POMDPs. The \texttt{zeroconf} model  has a derivative that has an
amplitude that is lower than the precision, and thus our implementation of Gradient Descent cannot
find the optimum. On some other case studies, the Parameter Lifting method performs better than
Gradient Descent.

\begin{table}
\begin{adjustbox}{width=\textwidth}

\begin{threeparttable}

% BEGIN COPYING HERE ==========
\begin{tabular}{llrrrrrr}
\toprule
Model & Spec & \multicolumn{4}{c}{Time (s)} & \multicolumn{2}{c}{Found Probability} \\
\cmidrule(r){3-6}
\cmidrule(r){7-8}
&& \multicolumn{2}{c}{GD} & \multicolumn{2}{c}{PL} &
\multicolumn{1}{c}{GD\tnote{1}} & \multicolumn{1}{c}{PL} \\
\cmidrule(r){3-4}
\cmidrule(r){5-6}
&& \multicolumn{1}{c}{\(\varnothing\)} &
\multicolumn{1}{c}{\(\sigma\)} & \multicolumn{1}{c}{\(\varnothing\)} &
\multicolumn{1}{c}{\(\sigma\)} &&
\\
\midrule
\texttt{brp} & P$\ge$0.01 & \(6.3994\)&\(2.4016\) & \(4.6196\)&\(0.0624\) & \(0.0301\) & \(0.0423\) \\
\ditto & P$\ge$0.3 & \multicolumn{2}{c}{TO} & \multicolumn{2}{c}{TO} & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{N/A} \\
\ditto & P$\ge$0.7 & \multicolumn{2}{c}{TO} & \(848.8825\)\tnote{2} & \(0.2795\) & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{NF} \\
\ditto & P$\ge$0.99 & \multicolumn{2}{c}{TO} & \(386.5212\)&\(39.8214\) & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{NF} \\
\texttt{crowds} & P$\ge$0.01  & \(0.0472\)&\(0.0010\) & \(0.0516\)&\(0.0015\) & \(0.9019\) & \(1.0000\) \\
\ditto & P$\ge$0.3  & \(0.0566\)&\(0.0172\) & \(0.0526\)&\(0.0014\) & \(0.9329\) & \(1.0000\) \\
\ditto & P$\ge$0.7  & \(0.0684\)&\(0.0197\) & \(0.0554\)&\(0.0026\) & \(0.9147\) & \(1.0000\) \\
\ditto & P$\ge$0.99  & \(0.0776\)&\(0.0247\) & \(0.0516\)&\(0.0034\) & \(0.9951\) & \(1.0000\) \\
\texttt{crypt4-max} & P$\ge$0.01  & \(1.4190\)&\(0.0148\) & \multicolumn{2}{c}{ERR} & \(0.3333\) & \multicolumn{1}{c}{N/A} \\
\ditto & P$\ge$0.3  & \(1.2722\)&\(0.0290\) & \multicolumn{2}{c}{ERR} & \(0.3333\) & \multicolumn{1}{c}{N/A} \\
\ditto & P$\ge$0.7  & \multicolumn{2}{c}{TO} & \multicolumn{2}{c}{ERR} & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{N/A} \\
\ditto & P$\ge$0.99  & \multicolumn{2}{c}{TO} & \multicolumn{2}{c}{ERR} & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{N/A} \\
\texttt{crypt4-min} & P$\le$0.01  & \multicolumn{2}{c}{TO} & \multicolumn{2}{c}{ERR} & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{N/A} \\
\ditto & P$\le$0.3  & \multicolumn{2}{c}{TO} & \multicolumn{2}{c}{ERR} & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{N/A} \\
\ditto & P$\le$0.7  & \(1.3044\)&\(0.0135\) & \multicolumn{2}{c}{ERR} & \(0.3333\) & \multicolumn{1}{c}{N/A} \\
\ditto & P$\le$0.99  & \(1.2740\)&\(0.0300\) & \multicolumn{2}{c}{ERR} & \(0.3333\) & \multicolumn{1}{c}{N/A} \\
\texttt{herman} & P$\ge$0.01  & \(33.5872\)&\(0.0660\) & \(11.6448\)&\(0.1226\) & \(0.7208\) & \(0.0533\) \\
\ditto & P$\ge$0.3  & \(44.1784\)&\(12.8330\) & \(13.9546\)&\(0.1532\) & \(0.7834\) & \(0.8278\) \\
\ditto & P$\ge$0.7  & \(39.8780\)&\(10.8047\) & \(13.6886\)&\(0.3105\) & \(0.7700\) & \(0.8278\) \\
\ditto & P$\ge$0.99  & \multicolumn{2}{c}{TO} & \(17.6154\)&\(0.9951\) & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{NF} \\
\texttt{nand} & P$\ge$0.01  & \(1.7352\)&\(0.4441\) & \(1.8820\)&\(0.0098\) & \(0.3009\) & \(1.0000\) \\
\ditto & P$\ge$0.3  & \(2.4590\)&\(0.3854\) & \(1.9072\)&\(0.0350\) & \(0.8436\) & \(1.0000\) \\
\ditto & P$\ge$0.7  & \(2.4296\)&\(0.4261\) & \(1.9278\)&\(0.0375\) & \(0.9802\) & \(1.0000\) \\
\ditto & P$\ge$0.99  & \(2.6306\)&\(0.3494\) & \(1.9534\)&\(0.0083\) & \(1.0000\) & \(1.0000\) \\
\texttt{newgrid} & P$\ge$0.01  & \(0.0064\)&\(0.0008\) & \(0.0052\)&\(0.0004\) & \(0.4100\) & \(0.5000\) \\
\ditto & P$\ge$0.3  & \(0.0070\)&\(0.0011\) & \(0.0052\)&\(0.0004\) & \(0.6072\) & \(0.5000\) \\
\ditto & P$\ge$0.7  & \(0.0156\)&\(0.0055\) & \(0.0236\)&\(0.0005\) & \(0.7875\) & \(0.7446\) \\
\ditto & P$\ge$0.99  & \(0.0710\)&\(0.0532\) & \multicolumn{2}{c}{TO} & \(0.9950\) & \multicolumn{1}{c}{N/A} \\
\texttt{nrp} & P$\ge$0.01  & \(0.0070\)&\(0.0000\) & \(2.2278\)&\(0.0157\) & \(0.0500\) & \(0.0500\) \\
\ditto & P$\ge$0.3  & \multicolumn{2}{c}{TO} & \multicolumn{2}{c}{MO} & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{N/A} \\
\ditto & P$\ge$0.7  & \multicolumn{2}{c}{TO} & \(767.1502\)&\(8.2997\) & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{NF} \\
\ditto & P$\ge$0.99  & \multicolumn{2}{c}{TO} & \(769.4190\)&\(6.3128\) & \multicolumn{1}{c}{N/A} & \multicolumn{1}{c}{NF} \\
\texttt{zeroconf}\tnote{3} & P$\ge$0.01  & \(4.1660\)&\(0.5756\)  & \(1.6674\)&\(0.0141\) & \(0.6598\) & \(1.0000\) \\
\ditto & P$\ge$0.3  & \(5.0010\)&\(3.0927\) & \(1.6476\)&\(0.0258\) & \(0.7390\) & \(1.0000\) \\
\ditto & P$\ge$0.7  & \(3.2196\)&\(0.6161\) & \(1.6260\)&\(0.0075\) & \(0.9367\) & \(1.0000\) \\
\ditto & P$\ge$0.99  & \(3.7872\)&\(0.9215\) & \(1.6270\)&\(0.0327\) & \(1.0000\)  & \(1.0000\) \\
\bottomrule
\end{tabular}
% END COPYING HERE ============

\begin{tablenotes}
\item[1] Gradient Descent found different probabilities when satisfying bounds. The table shows the
	average probability, we have checked that all probablities are correct answers.
\item[2] Parameter Lifting timed out during three of five measurements of this benchmark.
\item[3] We had to adjust the precision of the solver and use rational numbers so that Gradient
	Descent could find the maximum in the \texttt{zeroconf} model. We did not adjust these
	for Parameter Lifting.
\end{tablenotes}
		
\end{threeparttable}
\end{adjustbox}
\caption{Benchmarks finding a feasible instantiation}
\label{fig:benchmarks_feasible}
\end{table}
