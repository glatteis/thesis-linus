\section{Formal problem statements and method outline}\label{section:problem}

In this section, we first the discuss the exact problem statements and then elaborate on the general approach we
take to solve these problems.

\subsection{Problem description}

In a pMC \(\MC\), the \emph{reachability function} \(\Pr_{\MC}(s_0 \vDash \diamondsuit \good)\) from some
state \(s_0\) to a target state \(\good\) is a rational function over the set of parameters \(V\)
that describes the probability that a path eventually reaches \(\good\) from the initial
state \(s_0\).  The \emph{pMC extremum problem} asks for an instantiation of \(\MC\) that maximizes
or minimizes the reachability function.  The \emph{feasible instantiation problem}
asks for an instantiation that fulfills some reachability specification \(\Prob_{\leq
\lambda}(\diamondsuit \good)\) or \(\Prob_{\geq \lambda}(\diamondsuit \good)\) for \(\lambda \in \mathbb{R}\).

\begin{definition}[pMC extremum problem]\label{def:pmc_extremum}
	Given a pMC \(\MC = (S, s_0, \Prob, AP, L)\) and a target \(\good\),
	find an instantiation \(u: V \rightarrow \mathbb{R}\) that minimizes (or maximizes)
	\(\Pr_{\MC}(s_0 \vDash \diamondsuit \good)\).
\end{definition}

\begin{example}
	Given the pMC in \cref{fig:gradient_construction_mc}, the instantiation \(u: \{p\}
	\rightarrow \mathbb{R}\) with \(p \mapsto 0.5\) maximizes \(\Pr_{\MC}(s_0 \vDash
	\diamondsuit \good)\).
\end{example}

\begin{definition}[Feasible instantiation problem]\label{def:pmc_synthesis}
	Given a pMC \(\MC = (S, s_0, \Prob, AP, L)\) and a reachability specification
	\(\varphi_r = \Prob_{\leq \lambda} (\diamondsuit \good)\) or
	\(\varphi_r = \Prob_{\geq \lambda} (\diamondsuit \good)\)
	for \(\lambda \in \mathbb{R}\),
	find a well-defined instantiation
	\(u: V \rightarrow \mathbb{R}\) for \(\MC\) such that \(\MC[u] \vDash \varphi_r\).
\end{definition}

\begin{example}
	Given the pMC in \cref{fig:gradient_construction_mc} and the reachability specification \(\Prob_{\geq 0.2}(\diamondsuit \good)\),
	a valid result of the feasible instantiation problem would be any instantiation
	\(u: \{p\} \rightarrow \mathbb{R}\) with \(u(p) \cdot (1-u(p)) \geq 0.2\).
\end{example}

\begin{remark}
	The feasible instantiation problem is sometimes called the \emph{Parameter synthesis
	problem}, although this term is also used for the formulation of the problem where it is
	asked if \(\MC[u] \vDash \varphi_r\) for all instantiations \(u\) (like in
	\cite{parameter-synthesis}).
\end{remark}

\subsection{Existing approaches}

First, we discuss two approaches to solve the problems above that are already found in literature.

\paragraph{Computing the reachability function explicitly} This approach computes the reachability
function \(\Pr_\MC(s_0 \vDash \diamondsuit \good)\) explicitly. By applying optimization methods
directly on the rational function, one can then solve the feasible instantiation problem or the pMC
extremum problem. A problem with this method is that the length of \(\Pr_\MC(s_0 \vDash \diamondsuit
\good)\) is exponential in the number of parameters of the pMC \cite[124]{sebastiandiss}.  In
reality, the size of the reachability function is a practical problem \cite{fasterthanever}. This
approach works efficiently for pMCs with a few million states and two parameters \cite{landscape}.

\paragraph{Parameter Lifting}\label{section:parameter_lifting}
\emph{Parameter Lifting} \cite{fasterthanever} classifies \emph{regions} in the
parameter space as \emph{safe} or \emph{unsafe} based on a reachability specification \(\varphi_r\).
A region is a set of instantiations with bounds on
the parameters, for instance for the parameters \(V = \{p, q\}\), the set of instantiations \(u: V
\rightarrow \mathbb{R}\) with \(0.2 \leq u(p) \leq 0.5\) and \(0.35 \leq u(q) \leq 0.7\) is a region.
A region is \emph{safe} if all instantiations in the region satisfy \(\varphi_r\)
and it is \emph{unsafe} if no instantiation in the region satisfies \(\varphi_r\).

When one wants to search for a solution to the feasible instantiation problem using the Parameter
Lifting algorithm, one can use a divide-and-conquer approach: Start with the region that is the
entire parameter space and use the Parameter Lifting algorithm on this region. The Parameter Lifting
algorithm then tries to prove this region safe or unsafe. This fails if the region is neither
safe nor unsafe, so the result might be inconclusive. When the result is inconclusive,
split up the region and restart the algorithm on the newly created regions. Once a safe region is
found, one can pick out any instantiation as a feasible instantiation.  This divide-and-conquer
approach can be modified to find a solution to the pMC extremum problem.

While Parameter Lifting can be applied to the feasible instantiation problem, it can not only be
used to find \emph{one} instantiation that satisfies \(\varphi_r\) but regions of instantiations
that satisfy it. Approaches using Parameter Lifting thus lend itself to calculations like deciding
whether an instantiation satisifies \(\varphi_r\) for 95\% of the instantiations in the parameter
space (by some measure).

\subsection{Gradient Descent on pMCs}

We now discuss the approach we take to solve the feasible instantiation problem and the pMC extremum
problem. We do this by finding a (local) optimum (maximum or minimum) in the reachability function
\(\Pr(s_0 \vDash \diamondsuit \good)\) using \textit{Gradient Descent}.
Starting at some instantiation \(u: V \rightarrow \mathbb{R}\), Gradient Descent computes the
derivative
\(\dervar{p} \Pr(s_0 \vDash \diamondsuit \good)\) with respect to some or all parameters \(p \in V\).
This yields information about the direction in which a local optimum of the function can be found.
The Gradient Descent algorithm then steps into this direction and repeats this process until
either \(\Pr_{\MC[u]}(s_0 \vDash \diamondsuit \good)\) satisfies some threshold or has not
decreased (or increased) significantly in the last \(N\) steps.

\begin{remark}
	For brevity, we write \(\dervar{p} = \frac{\partial}{\partial p}\).
\end{remark}

Performing Gradient Descent on a pMC \(\MC\) requires finding the value of both the reachability
function \(\Pr_{\MC}(s_0 \vDash \diamondsuit \good)\) and the derived reachability function
\(\dervar{p} \Pr_{\MC}(s_0 \vDash \diamondsuit \good)\) at certain instantiations \(u: V \rightarrow
\mathbb{R}\).  We first show how to compute \(\Pr(s_0 \vDash \diamondsuit \good)\) using a system of
equations in \cref{section:exact}.  From this system of equations, there is a well-known and
efficient way to approximate the reachability function at given instantiations without computing
it exactly.

Next, we need to compute the derivative of the reachability function \(\dervar{p} \Pr(s_0 \vDash
\diamondsuit \good)\).  One could do this by computing the reachability function and then deriving
it. This has the problem that the length of the reachability function is asymptotically exponential
in the the number of parameters. Thus, we avoid this approach by directly
deriving the system of equations for the reachability function in \cref{section:construction}. We
then instantiate this system of equations at the points we are interested in and solve it
numerically. These two approaches are illustrated in \cref{fig:two_approaches}: Taking the left path
includes an exponential blowup. Taking the right path avoids this blowup.

% HACK: For some reason the smiley is too low in this figure. Make a command that raises it
% upwards.
\newcommand\upgood{\raisebox{\depth}{$\good$}}
\begin{figure}
\begin{center}
\begin{tikzcd}
\text{pMC } \MC \arrow[d] \arrow[d, "\text{Construct Equation System (\cref{section:exact})}" description]                                                                  &                                                                                                                      \\
\text{Equation System for } \Pr(s_0 \vDash \diamondsuit \upgood) \text{ in } \MC \arrow[rd, "\text{Derive
(\cref{section:construction})}" description] \arrow[red, d, "\text{Solve explicitly (exponential
blowup)}" description] &                                                                                                                      \\
\Pr(s_0 \vDash \diamondsuit \upgood) \text{ in } \MC \arrow[d, "\text{Derive}" description]
& \text{Equation System for } \dervar{p} \Pr(s \vDash \diamondsuit \upgood) \text{ in } \MC
\arrow[d, "\text{Instantiate with \(u\)}" description] \\ \dervar{p} \Pr(s_0 \vDash \diamondsuit
\upgood) \text{ in } \MC \arrow[d, "\text{Instantiate with \(u\)}" description]
&
\text{Equation System for } \dervar{p} \Pr(s \vDash \diamondsuit \upgood) \text{ in } \MC[u]
\arrow[ld, "\text{Solve numerically}" description]                                               \\
	\dervar{p} \Pr(s_0 \vDash \diamondsuit \upgood) \text{ in } \MC[u]                                                                      &                                                                                                                     
\end{tikzcd}
\end{center}
\caption{Two approaches to compute the derived reachability function}\label{fig:two_approaches}
\end{figure}
