import glob
import subprocess
import argparse
from datetime import datetime
from os import path, makedirs

parser = argparse.ArgumentParser(description='Run visualizations on all test cases.')
parser.add_argument('--binary', type=str, default="~/git/storm/build/bin/storm-pars",
                    help='location of the storm-pars binary')
args = parser.parse_args()

current_time = datetime.now().strftime("%Y-%b-%d-%H-%M-%S")
output_folder = "output/" + current_time + "/"

for folder in glob.glob("testcases/*"):
    name = folder.split("/")[1]

    maybe_drn_file = glob.glob(folder + "/*.drn")
    maybe_prism_file = glob.glob(folder + "/*.pm")
    maybe_prism_file.extend(glob.glob(folder + "/*.prism"))
    is_drn = len(maybe_drn_file) > 0
    actual_file = ""
    actual_command = ""
    if is_drn:
        actual_file = maybe_drn_file[0]
        actual_command = "--explicit-drn"
    else:
        actual_file = maybe_prism_file[0]
        actual_command = "--prism"

    prop_file = glob.glob(folder + "/*.props")[0]

    makedirs(path.join(output_folder, name), exist_ok=False)

    command = "{binary} {command} {file} --prop {prop_file} --derivatives  --core:eqsolver topological --topological:eqsolver eigen".format(
        binary = args.binary,
        command = actual_command,
        file = actual_file,
        prop_file = prop_file,
    )

    print("Running storm on {}...".format(name))
    output = subprocess.check_output(command, shell=True).decode()

    # Find the line with json - it starts with [{
    json_line = list(filter(lambda x: x.startswith("[{"), output.split("\n")))[0]
    # Save json line to output
    json_file = path.join(output_folder, name, "out.json")
    with open(json_file, "w") as f:
        f.write(json_line)

    rscript_command = "Rscript descent_visualization.r {} {}".format(json_file, path.join(output_folder, name, "out.pdf"))
    print("Running R on output...")
    subprocess.run(rscript_command, shell=True)

    print("Finished with {}.".format(name))
print("Results in {}.".format(output_folder))
