library(tidyr)
library(jsonlite)
library(ggplot2)
library(reshape2)
library(dplyr)

args = commandArgs(trailingOnly=TRUE)

run <- fromJSON(args[1], flatten=TRUE)
# show(run)

# xVar <- names(run)[2]
# yVar <- names(run)[7]
# ggplot(data = run, mapping = aes_string(x = xVar, y = yVar, color = "probability")) +
#   geom_path(
#     size = 1,
#     arrow = arrow(type = "closed", angle = 50, length = unit(0.05, "inches"))
#   ) +
#   xlim(0, 1) +
#   ylim(0, 1)


run$time <- as.numeric(row.names(run))

run <- melt(run, id.vars = 'time', variable.name = 'series')

runNoProb <- dplyr::filter(run, series != 'probability')
runProb <- dplyr::filter(run, series == 'probability')

plot <- ggplot(runProb, mapping = aes(y = value, x = time, color = series)) +
  geom_path(
    data = runNoProb,
    size = 1,
    arrow = arrow(type = "closed", angle = 50, length = unit(0.05, "inches"))
  ) +
  geom_path(
    data = runProb,
    size = 2,
    arrow = arrow(type = "closed", angle = 50, length = unit(0.05, "inches"))
  ) +
  labs(color = "Parameters") +
  ylim(0, 1) +
  xlab("Step number") +
  ylab("Value of parameter")

ggsave(plot = plot, width = 14, height = 5, limitsize=FALSE, dpi = 250, filename = args[2])
